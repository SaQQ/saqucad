﻿using System.Windows;
using SaQuCAD.Shapes.Surfaces.CreationViewModels;

namespace SaQuCAD
{
	public partial class ControlpointsSurfaceCreationWindow : Window
	{

		#region Constructors

		public ControlpointsSurfaceCreationWindow(ControlpointsBaseCreationVM viewModel)
		{
			InitializeComponent();

			DataContext = this.viewModel = viewModel;

			viewModel.EditingDone += EditingDone;
			Closing += WindowClosing;			
		}

		#endregion

		#region Private Helpers

		private void EditingDone()
		{
			Closing -= WindowClosing;
			Close();
		}

		private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			viewModel.EditingDone -= EditingDone;
			viewModel.Reject.Execute(null);
		}

		#endregion

		#region Private Data

		private ControlpointsBaseCreationVM viewModel;
		
		#endregion

	}
}
