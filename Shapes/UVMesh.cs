﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Shapes
{
	public abstract class UVMesh : Mesh, IUVParametrized
	{

		#region Public Properties

		public int USegments { get { return uSegments; } set { uSegments = value; OnUSegmentsChange(); RaisePropertyChanged(); OnChanged(); } }

		public int VSegments { get { return vSegments; } set { vSegments = value; OnVSegmentsChange(); RaisePropertyChanged(); OnChanged(); } }

		public abstract double UMin { get; }

		public abstract double UMax { get; }

		public abstract double VMin { get; }

		public abstract double VMax { get; }

		public abstract ParamProps UProperties { get; }

		public abstract ParamProps VProperties { get; }

		public ObservableCollection<ITrimmer> Trimmers { get; private set; } = new ObservableCollection<ITrimmer>();

		public TrimMode TrimMode { get { return trimMode; } set { trimMode = value; RecreateMesh(); RaisePropertyChanged(); } }

		#endregion

		#region Constructors

		public UVMesh()
		{
			Trimmers.CollectionChanged += TrimmersCollectionChanged;
		}

		#endregion

		#region Public Methods

		public abstract Vector3 Evaluate(double u, double v);

		public abstract Vector3 EvaluateUDerivative(double u, double v);

		public abstract Vector3 EvaluateVDerivative(double u, double v);

		#endregion

		#region Protected Methods

		protected virtual void OnUSegmentsChange()
		{
			RecreateMesh();
		}

		protected virtual void OnVSegmentsChange()
		{
			RecreateMesh();
		}

		protected void RecreateMesh()
		{
			Vertices.Clear();
			Indices.Clear();

			var du = (UMax - UMin) / uSegments;
			var dv = (VMax - VMin) / vSegments;

			bool[,] tests = new bool[uSegments + 1, vSegments + 1];
			int[,] indices = new int[uSegments + 1, vSegments + 1];
			int[,] uIndices = new int[uSegments, vSegments + 1];
			int[,] vIndices = new int[uSegments + 1, vSegments];

			int ind = 0;

			Func<Vector2, Vector2, Vector2> converge = (Vector2 a, Vector2 b) =>
			{
				var at = Test(a.x, a.y);
				var bt = Test(b.x, b.y);

				for (int i = 0; i < 5; ++i)
				{
					var mid = (a + b) / 2.0;
					var mt = Test(mid.x, mid.y);

					if (mt ^ at)
						b = mid;
					else
						a = mid;
				}

				return a;
			};

			for (int u = 0; u < uSegments + 1; ++u)
			{
				for (int v = 0; v < vSegments + 1; ++v)
				{
					tests[u, v] = Test(UMin + u * du, VMin + v * dv);

					if (tests[u, v])
					{
						Vertices.Add(Evaluate(UMin + u * du, VMin + v * dv));
						indices[u, v] = ind;
						++ind;
					}

				}
			}

			for (int u = 0; u < uSegments + 1; ++u)
			{
				for (int v = 0; v < vSegments + 1; ++v)
				{

					if (u > 0 && u < uSegments + 1 && tests[u, v] ^ tests[u - 1, v])
					{
						var c = converge(new Vector2(UMin + (u - 1) * du, VMin + v * dv), new Vector2(UMin + u * du, VMin + v * dv));
						Vertices.Add(Evaluate(c.x, c.y));
						uIndices[u - 1, v] = ind;
						++ind;
					}

					if (v > 0 && v < vSegments + 1 && tests[u, v] ^ tests[u, v - 1])
					{
						var c = converge(new Vector2(UMin + u * du, VMin + (v - 1) * dv), new Vector2(UMin + u * du, VMin + v * dv));
						Vertices.Add(Evaluate(c.x, c.y));
						vIndices[u, v - 1] = ind;
						++ind;
					}

				}
			}

			for (int u = 0; u < uSegments; ++u)
			{
				for (int v = 0; v < vSegments; ++v)
				{
					var u0v0 = tests[u, v];
					var u1v0 = tests[u + 1, v];
					var u0v1 = tests[u, v + 1];
					var u1v1 = tests[u + 1, v + 1];

					var type = (u0v0 ? 1 : 0) | (u1v0 ? 2 : 0) | (u1v1 ? 4 : 0) | (u0v1 ? 8 : 0);

					switch (type)
					{
						case 0/*0b0000*/:
							break;
						case 15/*0b1111*/:
							Indices.Add(Tuple.Create(indices[u, v], indices[u + 1, v]));
							Indices.Add(Tuple.Create(indices[u + 1, v], indices[u + 1, v + 1]));
							Indices.Add(Tuple.Create(indices[u + 1, v + 1], indices[u, v + 1]));
							Indices.Add(Tuple.Create(indices[u, v + 1], indices[u, v]));
							break;
						case 14/*0b1110*/:
							Indices.Add(Tuple.Create(uIndices[u, v], indices[u + 1, v]));
							Indices.Add(Tuple.Create(indices[u + 1, v], indices[u + 1, v + 1]));
							Indices.Add(Tuple.Create(indices[u + 1, v + 1], indices[u, v + 1]));
							Indices.Add(Tuple.Create(vIndices[u, v], indices[u, v + 1]));
							Indices.Add(Tuple.Create(vIndices[u, v], uIndices[u, v]));
							break;
						case 13/*0b1101*/:
							Indices.Add(Tuple.Create(indices[u, v], uIndices[u, v]));
							Indices.Add(Tuple.Create(uIndices[u, v], vIndices[u + 1, v]));
							Indices.Add(Tuple.Create(vIndices[u + 1, v], indices[u + 1, v + 1]));
							Indices.Add(Tuple.Create(indices[u + 1, v + 1], indices[u, v + 1]));
							Indices.Add(Tuple.Create(indices[u, v + 1], indices[u, v]));
							break;
						case 11/*0b1011*/:
							Indices.Add(Tuple.Create(indices[u, v], indices[u + 1, v]));
							Indices.Add(Tuple.Create(indices[u + 1, v], vIndices[u + 1, v]));
							Indices.Add(Tuple.Create(vIndices[u + 1, v], uIndices[u, v + 1]));
							Indices.Add(Tuple.Create(uIndices[u, v + 1], indices[u, v + 1]));
							Indices.Add(Tuple.Create(indices[u, v + 1], indices[u, v]));
							break;
						case 7/*b0111*/:
							Indices.Add(Tuple.Create(indices[u, v], indices[u + 1, v]));
							Indices.Add(Tuple.Create(indices[u + 1, v], indices[u + 1, v + 1]));
							Indices.Add(Tuple.Create(indices[u + 1, v + 1], uIndices[u, v + 1]));
							Indices.Add(Tuple.Create(uIndices[u, v + 1], vIndices[u, v]));
							Indices.Add(Tuple.Create(vIndices[u, v], indices[u, v]));
							break;
						case 1/*0b0001*/:
							Indices.Add(Tuple.Create(indices[u, v], uIndices[u, v]));
							Indices.Add(Tuple.Create(uIndices[u, v], vIndices[u, v]));
							Indices.Add(Tuple.Create(vIndices[u, v], indices[u, v]));
							break;
						case 2/*0b0010*/:
							Indices.Add(Tuple.Create(uIndices[u, v], indices[u + 1, v]));
							Indices.Add(Tuple.Create(indices[u + 1, v], vIndices[u + 1, v]));
							Indices.Add(Tuple.Create(vIndices[u + 1, v], uIndices[u, v]));
							break;
						case 4/*0b0100*/:
							Indices.Add(Tuple.Create(vIndices[u + 1, v], indices[u + 1, v + 1]));
							Indices.Add(Tuple.Create(indices[u + 1, v + 1], uIndices[u, v + 1]));
							Indices.Add(Tuple.Create(uIndices[u, v + 1], vIndices[u + 1, v]));
							break;
						case 8/*0b1000*/:
							Indices.Add(Tuple.Create(uIndices[u, v + 1], indices[u, v + 1]));
							Indices.Add(Tuple.Create(indices[u, v + 1], vIndices[u, v]));
							Indices.Add(Tuple.Create(vIndices[u, v], uIndices[u, v + 1]));
							break;
						case 12/*0b1100*/:
							Indices.Add(Tuple.Create(vIndices[u + 1, v], indices[u + 1, v + 1]));
							Indices.Add(Tuple.Create(indices[u + 1, v + 1], indices[u, v + 1]));
							Indices.Add(Tuple.Create(indices[u, v + 1], vIndices[u, v]));
							Indices.Add(Tuple.Create(vIndices[u, v], vIndices[u + 1, v]));
							break;
						case 9/*0b1001*/:
							Indices.Add(Tuple.Create(indices[u, v], uIndices[u, v]));
							Indices.Add(Tuple.Create(uIndices[u, v], uIndices[u, v + 1]));
							Indices.Add(Tuple.Create(uIndices[u, v + 1], indices[u, v + 1]));
							Indices.Add(Tuple.Create(indices[u, v + 1], indices[u, v]));
							break;
						case 3/*0b0011*/:
							Indices.Add(Tuple.Create(indices[u, v], indices[u + 1, v]));
							Indices.Add(Tuple.Create(indices[u + 1, v], vIndices[u + 1, v]));
							Indices.Add(Tuple.Create(vIndices[u + 1, v], vIndices[u, v]));
							Indices.Add(Tuple.Create(vIndices[u, v], indices[u, v]));
							break;
						case 6/*0b0110*/:
							Indices.Add(Tuple.Create(uIndices[u, v], indices[u + 1, v]));
							Indices.Add(Tuple.Create(indices[u + 1, v], indices[u + 1, v + 1]));
							Indices.Add(Tuple.Create(indices[u + 1, v + 1], uIndices[u, v + 1]));
							Indices.Add(Tuple.Create(uIndices[u, v + 1], uIndices[u, v]));
							break;

					}
				}
			}

			OnChanged();
		}

		#endregion

		#region Private Helpers

		private void TrimmersCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if (e.NewItems != null)
			{
				foreach (var item in e.NewItems)
					((ITrimmer)item).Changed += TrimmerChanged;
			}

			if (e.OldItems != null)
			{
				foreach (var item in e.NewItems)
					((ITrimmer)item).Changed += TrimmerChanged;
			}

			RecreateMesh();
		}

		private void TrimmerChanged()
		{
			RecreateMesh();
		}

		private bool Test(double u, double v)
		{
			if (trimMode == TrimMode.Each)
			{
				foreach (var trimmer in Trimmers)
					if (!trimmer.Test(u, v))
						return false;

				return true;
			}
			else
			{
				foreach (var trimmer in Trimmers)
					if (trimmer.Test(u, v))
						return true;

				return false;
			}
		}

        #endregion

        #region Private Data

        private int uSegments = 20, vSegments = 20;

		private TrimMode trimMode = TrimMode.Each;

		#endregion


	}
}
