﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;
using System;
using System.Collections.Generic;

namespace SaQuCAD.Shapes
{
	public class Point : SceneObject, ITranslatable
    {

		#region Public Properties

		public double X { get { return position.x; } set { position.x = value; RaisePropertyChanged(); RaisePropertyChanged(nameof(Position)); OnChanged(); } }

		public double Y { get { return position.y; } set { position.y = value; RaisePropertyChanged(); RaisePropertyChanged(nameof(Position)); OnChanged(); } }

		public double Z { get { return position.z; } set { position.z = value; RaisePropertyChanged(); RaisePropertyChanged(nameof(Position)); OnChanged(); } }

        public Vector3 Position { get { return position; } set { position = value; RaisePropertyChanged(); RaisePropertyChanged(nameof(X)); RaisePropertyChanged(nameof(Y)); RaisePropertyChanged(nameof(Z)); OnChanged(); } }

		#endregion

		#region Constructors

		public Point(double x, double y, double z, string name = "New Point")
		{
			Name = name;
			position = new Vector3(x, y, z);
		}

		public Point(Vector3 pos, string name = "New Point")
		{
			Name = name;
			this.position = pos;
		}

		#endregion

		#region Public Methods

		public override void Render(Renderer renderer)
        {
			if (!Visible)
				return;
			renderer.DrawPoint(position, ColorCode);
        }

		public Point MirroredThroughPlane(Plane plane)
		{
			switch (plane)
			{
				case Plane.XY:
					return new Point(new Vector3(position.x, position.y, -position.z), "Mirrored " + Name);
				case Plane.XZ:
					return new Point(new Vector3(position.x, -position.y, position.z), "Mirrored " + Name);
				case Plane.YZ:
					return new Point(new Vector3(-position.x, position.y, position.z), "Mirrored " + Name);
				default:
					throw new ArgumentException("Unknown plane", nameof(plane));
			}
		}

		public static explicit operator GM1.Serialization.Point(Point p)
		{
			return new GM1.Serialization.Point()
			{
				Name = p.Name,
				Position = new GM1.Serialization.Vector3()
				{
					X = (float)p.X,
					Y = (float)p.Y,
					Z = (float)p.Z
				}
			};
		}

		public override IEnumerable<SceneObject> Mirror(Plane plane)
		{
			yield return MirroredThroughPlane(plane);
		}

		public override string ToString()
		{
			return string.Format("({0:0.00}, {1:0.00}, {2:0.00})", position.x, position.y, position.z);
		}

		#endregion

		#region Private Data

		private Vector3 position;

		#endregion

	}
}
