﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Shapes
{
    public abstract class AffineUVMesh : UVMesh, ITranslatable
    {

        #region Public Properties

        public Matrix4x4 ModelMatrix { get; private set; } = Matrix4x4.Identity();

        public double X { get { return translation.v14; } set { translation.v14 = value; UpdateModelMatrix(); RaisePropertyChanged(); } }
        public double Y { get { return translation.v24; } set { translation.v24 = value; UpdateModelMatrix(); RaisePropertyChanged(); } }
        public double Z { get { return translation.v34; } set { translation.v34 = value; UpdateModelMatrix(); RaisePropertyChanged(); } }

        public double RX { get { return rx; } set { rx = value; updateRotation(); RaisePropertyChanged(); } }
        public double RY { get { return ry; } set { ry = value; updateRotation(); RaisePropertyChanged(); } }
        public double RZ { get { return rz; } set { rz = value; updateRotation(); RaisePropertyChanged(); } }

        public double SX { get { return scale.v11; } set { scale.v11 = value; UpdateModelMatrix(); RaisePropertyChanged(); } }
        public double SY { get { return scale.v22; } set { scale.v22 = value; UpdateModelMatrix(); RaisePropertyChanged(); } }
        public double SZ { get { return scale.v33; } set { scale.v33 = value; UpdateModelMatrix(); RaisePropertyChanged(); } }

        public Vector3 Position
        {
            get { return new Vector3(translation.v14, translation.v24, translation.v34); }
            set { translation.v14 = value.x; translation.v24 = value.y; translation.v34 = value.z; }
        }

        #endregion

        #region Private Helpers

        private void UpdateModelMatrix()
        {
            ModelMatrix = translation * rotation * scale;
			RecreateMesh();
        }

        private void updateRotation()
        {
            rotation = Matrix4x4.RotationX(rx) * Matrix4x4.RotationY(ry) * Matrix4x4.RotationZ(rz);
            UpdateModelMatrix();
        }

        #endregion 

        #region Private Data

        private Matrix4x4 translation = Matrix4x4.Identity();

        private double rx = 0.0, ry = 0.0, rz = 0.0;
        private Matrix4x4 rotation = Matrix4x4.Identity();

        private Matrix4x4 scale = Matrix4x4.Identity();

        #endregion

    }
}
