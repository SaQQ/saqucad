﻿using SaQuCAD.Maths;
using System;

namespace SaQuCAD.Shapes.Surfaces.CreationViewModels
{
	public class BezierC0CreationVM : ControlpointsBaseCreationVM
	{

		#region Constructors

		public BezierC0CreationVM(Scene scene) : base(scene)
		{
		}

		#endregion

		#region Protected Methods

		protected override void CreateSurface(Scene scene)
		{
			points = new Point[HeightSegments * 3 + 1, WidthSegments * 3 + 1];

			if (!Looped)
			{
				double dh = Height / (3 * HeightSegments);
				double dw = Width / (3 * WidthSegments);

				for (int i = 0; i < points.GetLength(0); ++i)
				{
					for (int j = 0; j < points.GetLength(1); ++j)
					{
						points[i, j] = new Point(LocalToGlobal(new Vector3(j * dw, i * dh, 0.0)));
						scene.AddObject(points[i, j]);
					}
				}
			}
			else
			{
				double dh = Height / (3 * HeightSegments);

				double optD = 4.0 * Math.Tan(Math.PI / (2.0 * WidthSegments)) / 3.0;

				for (int i = 0; i < points.GetLength(0); ++i)
				{
					if (WidthSegments > 1)
					{

						for (int seg = 0; seg < WidthSegments; ++seg)
						{
							double a = seg * 2.0 * Math.PI / WidthSegments;

							var vec = Width * new Vector2(Math.Cos(a), Math.Sin(a));
							var vecp = new Vector2(-vec.y, vec.x);

							var p2 = vec + vecp * optD;

							points[i, 3 * seg] = new Point(LocalToGlobal(new Vector3(vec.x, vec.y, i * dh)));
							points[i, 3 * seg + 1] = new Point(LocalToGlobal(new Vector3(p2.x, p2.y, i * dh)));

							a = (seg + 1) * 2.0 * Math.PI / WidthSegments;
							vec = Width * new Vector2(Math.Cos(a), Math.Sin(a));
							vecp = new Vector2(-vec.y, vec.x);

							var p3 = vec - vecp * optD;

							points[i, 3 * seg + 2] = new Point(LocalToGlobal(new Vector3(p3.x, p3.y, i * dh)));

							scene.AddObject(points[i, 3 * seg]);
							scene.AddObject(points[i, 3 * seg + 1]);
							scene.AddObject(points[i, 3 * seg + 2]);
						}

						points[i, WidthSegments * 3] = points[i, 0];
					}
					else
					{
						points[i, 0] = new Point(LocalToGlobal(new Vector3(Width, 0.0, i * dh)));
						points[i, 1] = new Point(LocalToGlobal(new Vector3(-5.0 * Width / 3.0, 3.0 * Width, i * dh)));
						points[i, 2] = new Point(LocalToGlobal(new Vector3(-5.0 * Width / 3.0, -3.0 * Width, i * dh)));
						points[i, 3] = points[i, 0];

						scene.AddObject(points[i, 0]);
						scene.AddObject(points[i, 1]);
						scene.AddObject(points[i, 2]);
					}
				}
			}

			surface = new BezierC0(points);
			scene.AddObject(surface);
		}

		protected override void RemoveSurface()
		{
			if (surface != null)
				surface.Remove.Execute(null);

			if (points != null)
				foreach (var point in points)
					point.Remove.Execute(null);
		}

		protected override void RepositionPoints()
		{
			if (!Looped)
			{
				double dh = Height / (3 * HeightSegments);
				double dw = Width / (3 * WidthSegments);

				for (int i = 0; i < points.GetLength(0); ++i)
					for (int j = 0; j < points.GetLength(1); ++j)
						points[i, j].Position = LocalToGlobal(new Vector3(j * dw, i * dh, 0.0));
			}
			else
			{
				double dh = Height / (3 * HeightSegments);

				double optD = 4.0 * Math.Tan(Math.PI / (2.0 * WidthSegments)) / 3.0;

				for (int i = 0; i < points.GetLength(0); ++i)
				{

					if (WidthSegments > 1)
					{

						for (int seg = 0; seg < WidthSegments; ++seg)
						{
							double a = seg * 2.0 * Math.PI / WidthSegments;

							var vec = Width * new Vector2(Math.Cos(a), Math.Sin(a));
							var vecp = new Vector2(-vec.y, vec.x);

							var p2 = vec + vecp * optD;

							points[i, 3 * seg].Position = LocalToGlobal(new Vector3(vec.x, vec.y, i * dh));
							points[i, 3 * seg + 1].Position = LocalToGlobal(new Vector3(p2.x, p2.y, i * dh));

							a = (seg + 1) * 2.0 * Math.PI / WidthSegments;
							vec = Width * new Vector2(Math.Cos(a), Math.Sin(a));
							vecp = new Vector2(-vec.y, vec.x);

							var p3 = vec - vecp * optD;

							points[i, 3 * seg + 2].Position = LocalToGlobal(new Vector3(p3.x, p3.y, i * dh));
						}

					}
					else
					{
						points[i, 0].Position = LocalToGlobal(new Vector3(Width, 0.0, i * dh));
						points[i, 1].Position = LocalToGlobal(new Vector3(-5.0 * Width / 3.0, 3.0 * Width, i * dh));
						points[i, 2].Position = LocalToGlobal(new Vector3(-5.0 * Width / 3.0, -3.0 * Width, i * dh));
					}
				}
			}
		}

		#endregion

		#region Private Data

		private Point[,] points;

		private BezierC0 surface;

		#endregion

	}
}
