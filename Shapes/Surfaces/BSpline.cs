﻿using System.Collections.Generic;
using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;
using SaQuCAD.Algorithms;

namespace SaQuCAD.Shapes.Surfaces
{
	public class BSpline : ControlpointsBase
	{

		#region Public Properties

		public override double UMin => 0.0;

		public override double UMax => UPatches;

        public override double VMin => 0.0;

		public override double VMax => VPatches;

		public int UPatches => ControlPoints.GetLength(0) - 3;

		public int VPatches => ControlPoints.GetLength(1) - 3;

        public override ParamProps UProperties => IsUCyclic(false) ? ParamProps.Cyclic : (IsUCyclic(true) ? ParamProps.Cyclic | ParamProps.Inverted : 0);

        public override ParamProps VProperties => IsVCyclic(false) ? ParamProps.Cyclic : (IsVCyclic(true) ? ParamProps.Cyclic | ParamProps.Inverted : 0);

		#endregion

		#region Constructors

		public BSpline(Point[,] points, string name = "New BSpline Surface") : base(points)
		{
			Name = name;
		}

		#endregion

		#region Public Methods

		public override Vector3 Evaluate(double u, double v)
		{
			int uPatch, vPatch;
			double pu, pv;
			DecomposeParameters(u, v, out uPatch, out vPatch, out pu, out pv);

			var bmat = DeBoor.Evaluate3Regular(pu).MatrixMultiply(DeBoor.Evaluate3Regular(pv));

			Vector3 value = new Vector3();

			for (int r = 0; r < 4; ++r)
				for (int c = 0; c < 4; ++c)
					value += bmat[r, c] * ControlPoints[uPatch + r, vPatch + c].Position;

			return value;
		}

		public override Vector3 EvaluateUDerivative(double u, double v)
		{
			int uPatch, vPatch;
			double pu, pv;
			DecomposeParameters(u, v, out uPatch, out vPatch, out pu, out pv);

			var bv = DeBoor.Evaluate3Regular(pv);

			Vector3[] w = new Vector3[4];

			for (int iu = 0; iu < 4; ++iu)
				for (int iv = 0; iv < 4; ++iv)
					w[iu] += bv[iv] * ControlPoints[uPatch + iu, vPatch + iv].Position;

			var bu = DeBoor.Evaluate2Regular(pu);

			Vector3 result = new Vector3();

			for (int i = 0; i < 3; ++i)
				result += (w[i + 1] - w[i]) * bu[i];

			return result;
		}

		public override Vector3 EvaluateVDerivative(double u, double v)
		{
			int uPatch, vPatch;
			double pu, pv;
			DecomposeParameters(u, v, out uPatch, out vPatch, out pu, out pv);

			var bu = DeBoor.Evaluate3Regular(pu);

			Vector3[] w = new Vector3[4];

			for (int iv = 0; iv < 4; ++iv)
				for (int iu = 0; iu < 4; ++iu)
					w[iv] += bu[iu] * ControlPoints[uPatch + iu, vPatch + iv].Position;

			var bv = DeBoor.Evaluate2Regular(pv);

			Vector3 result = new Vector3();

			for (int i = 0; i < 3; ++i)
				result += (w[i + 1] - w[i]) * bv[i];

			return result;
		}

		public override void Render(Renderer renderer)
		{
			if (FrameVisible)
				DrawFrame(renderer);

            base.Render(renderer);
		}

		public override IEnumerable<SceneObject> Mirror(Plane plane)
		{
			var mirroredPoints = GetMirroredPoints(plane);

			foreach (Point p in mirroredPoints)
				yield return p;

            // TODO: Load cyclicness
			yield return new BSpline(mirroredPoints, "Mirrored " + Name);
		}

		#endregion

		#region Private Helpers

		private bool IsUCyclic(bool reversed = false)
		{
			var U = ControlPoints.GetLength(0);
			var V = ControlPoints.GetLength(1);
			for (int v = 0; v < V; ++v)
				for (int u = 0; u < 2; ++u)
					if (ControlPoints[u, v] != ControlPoints[U - 3 + u, reversed ? V - 1 - v : v])
						return false;
			return true;
		}

		private bool IsVCyclic(bool reversed = false)
		{
			var U = ControlPoints.GetLength(0);
			var V = ControlPoints.GetLength(1);
			for (int u = 0; u < U; ++u)
				for (int v = 0; v < 2; ++v)
					if (ControlPoints[u, v] != ControlPoints[reversed ? U - 1 - u : u, V - 3 + v])
						return false;
			return true;
		}

		private void DecomposeParameters(double u, double v, out int uPatch, out int vPatch, out double pu, out double pv)
		{
            var uv = new Vector2(u, v);
            Wrapping.CraftWrapFunc(this)(ref uv);
            u = uv.x;
            v = uv.y;
			uPatch = (int)u == UPatches ? (int)u - 1 : (int)u;
			vPatch = (int)v == VPatches ? (int)v - 1 : (int)v;
			pu = u - uPatch;
			pv = v - vPatch;
        }

        #endregion

    }
}
