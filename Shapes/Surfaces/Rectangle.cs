﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;
using System.Collections.ObjectModel;

namespace SaQuCAD.Shapes.Surfaces
{
    public class Rectangle : SceneObject, IUVParametrized
	{

		#region Public Properties

		public int USegments { get { return uSegments;  } set { vSegments = value; RaisePropertyChanged(); OnChanged(); } }

		public int VSegments { get { return vSegments; } set { uSegments = value; RaisePropertyChanged(); OnChanged(); } }

		public double Width { get { return width; } set { width = value; RaisePropertyChanged(); OnChanged(); } }

		public double Height { get { return height; } set { height = value; RaisePropertyChanged(); OnChanged(); } }

		public Plane Plane { get { return plane; } set { plane = value; RaisePropertyChanged(); OnChanged(); } }

		public double UMin => 0.0;

		public double UMax => width;

		public double VMin => 0.0;

        public double VMax => height;

        public ParamProps UProperties => 0;

        public ParamProps VProperties => 0;

        public ObservableCollection<ITrimmer> Trimmers { get; private set; } = new ObservableCollection<ITrimmer>();

        public TrimMode TrimMode { get; set; }

        #endregion

        #region Constructors

        public Rectangle(string name = "New Rectangle")
		{
			Name = name;
		}

		#endregion

		#region Public Methods

		public Vector3 Evaluate(double u, double v)
		{
			switch(plane)
			{
				case Plane.XY:
					return new Vector3(u - width / 2.0, v - height / 2.0, 0.0);
				case Plane.XZ:
					return new Vector3(u - width / 2.0, 0.0, v - height / 2.0);
				case Plane.YZ:
					return new Vector3(0.0, u - width / 2.0, v - height / 2.0);
				default:
					return new Vector3();
			}
		}

		public Vector3 EvaluateUDerivative(double u, double v) {
			switch (plane)
			{
				case Plane.XY:
					return new Vector3(1.0, 0.0, 0.0);
				case Plane.XZ:
					return new Vector3(1.0, 0.0, 0.0);
				case Plane.YZ:
					return new Vector3(0.0, 1.0, 0.0);
				default:
					return new Vector3();
			}
		}

		public Vector3 EvaluateVDerivative(double u, double v)
		{
			switch (plane)
			{
				case Plane.XY:
					return new Vector3(0.0, 1.0, 0.0);
				case Plane.XZ:
					return new Vector3(0.0, 0.0, 1.0);
				case Plane.YZ:
					return new Vector3(0.0, 0.0, 1.0);
				default:
					return new Vector3();
			}
		}

		public override void Render(Renderer renderer)
		{
			double du = width / uSegments;
			double dv = height / vSegments;

			if (plane == Plane.XY)
			{
				for (int uSeg = 0; uSeg <= uSegments; ++uSeg)
					renderer.DrawSegment(
						new Vector3(uSeg * du - width / 2.0, -height / 2.0, 0.0),
						new Vector3(uSeg * du - width / 2.0, height / 2.0, 0.0),
						ColorCode);

				for (int vSeg = 0; vSeg <= vSegments; ++vSeg)
					renderer.DrawSegment(
						new Vector3(-width / 2.0, vSeg * dv - height / 2.0, 0.0),
						new Vector3(width / 2.0, vSeg * dv - height / 2.0, 0.0),
                        ColorCode);
			}
			else if (plane == Plane.XZ)
			{
				for (int uSeg = 0; uSeg <= uSegments; ++uSeg)
					renderer.DrawSegment(
						new Vector3(uSeg * du - width / 2.0, 0.0, -height / 2.0),
						new Vector3(uSeg * du - width / 2.0, 0.0, height / 2.0),
                        ColorCode);

				for (int vSeg = 0; vSeg <= vSegments; ++vSeg)
					renderer.DrawSegment(
						new Vector3(-width / 2.0, 0.0, vSeg * dv - height / 2.0),
						new Vector3(width / 2.0, 0.0, vSeg * dv - height / 2.0),
                        ColorCode);
			}
			else if(plane == Plane.YZ)
			{
				for (int uSeg = 0; uSeg <= uSegments; ++uSeg)
					renderer.DrawSegment(
						new Vector3(0.0, uSeg * du - width / 2.0, -height / 2.0),
						new Vector3(0.0, uSeg * du - width / 2.0, height / 2.0),
                        ColorCode);

				for (int vSeg = 0; vSeg <= vSegments; ++vSeg)
					renderer.DrawSegment(
						new Vector3(0.0, -width / 2.0, vSeg * dv - height / 2.0),
						new Vector3(0.0, width / 2.0, vSeg * dv - height / 2.0),
                        ColorCode);
			}

		}

		#endregion

		#region Private Data

		private int uSegments = 2;
		private int vSegments = 2;

		private double width = 1.0;
		private double height = 1.0;

		private Plane plane = Plane.XY;

		#endregion

	}
}
