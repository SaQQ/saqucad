﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;
using System.Collections.Generic;
using System.Linq;

namespace SaQuCAD.Shapes.Curves
{
	public class BSpline: ControlpointsBase, IFrameable, IEditable
	{
		public BSpline(Point[] points, string name = "New B-Spline") : base(points)
		{
			Name = name;
		}

		private bool frameVisible;

		public bool FrameVisible
		{
			get { return frameVisible; }
			set { frameVisible = value; RaisePropertyChanged(); OnChanged(); }
		}

        private bool drawBezierPolygon = false;

		public bool DrawBezierPolygon { get { return drawBezierPolygon; } set { drawBezierPolygon = value; OnChanged(); RaisePropertyChanged(); } }

		public struct BezierPolygon
		{
			public int DeBoorIndex { get; set; }

			public Vector3[] Points { get; set; }

			public bool StartCollapsed { get; set; }

			public bool EndCollapsed { get; set; }
		}

		public IEnumerable<BezierPolygon> BezierPolygons()
		{
			for (int i = 1; i < ControlPoints.Count - 2; ++i)
			{
				Vector3[] points = new Vector3[4];

				points[0] = ControlPoints[i].Position;
				points[3] = ControlPoints[i + 1].Position;

				points[1] = (2.0 * points[0] + 1.0 * points[3]) / 3.0;
				points[2] = (1.0 * points[0] + 2.0 * points[3]) / 3.0;

				if (i > 0)
				{
					// Not in first segment

					var prevm2 = (1.0 * ControlPoints[i - 1].Position + 2.0 * ControlPoints[i].Position) / 3.0;
					points[0] = prevm2;
					points[0] = (points[0] + points[1]) / 2.0;
				}
				if (i < ControlPoints.Count - 2)
				{
					// Not in last segment

					var nextm1 = (1.0 * ControlPoints[i + 2].Position + 2.0 * ControlPoints[i + 1].Position) / 3.0;
					points[3] = nextm1;
					points[3] = (points[3] + points[2]) / 2.0;
				}

				yield return new BezierPolygon() { DeBoorIndex = i, Points = points, StartCollapsed = i == 0, EndCollapsed = i == ControlPoints.Count - 1 };
			}
		}

		public override void Render(Renderer renderer)
		{
			if (!Visible)
				return;

			if (ControlPoints.Count < 4)
				return;

			if (frameVisible)
			{
				for (int i = 0; i < ControlPoints.Count - 1; ++i)
					renderer.DrawSegment(ControlPoints[i].Position, ControlPoints[i + 1].Position, ColorCodes.RenderFrame);
			}

			if (DrawBezierPolygon)
			{
				foreach (var bezierPolygon in BezierPolygons())
				{

					for (int i = 0; i < bezierPolygon.Points.Length - 1; ++i)
					{
						renderer.DrawSegment(bezierPolygon.Points[i], bezierPolygon.Points[i + 1], ColorCodes.RenderFrame);
						renderer.DrawPoint(bezierPolygon.Points[i], ColorCodes.RenderFrame);
					}

					renderer.DrawPoint(bezierPolygon.Points.Last(), ColorCodes.RenderFrame);
				}
			}

			var N = Algorithms.DeBoor.Evaluate3Regular(0.0);

			Vector3 lastPoint = ControlPoints[0].Position * N[0] + ControlPoints[1].Position * N[1] + ControlPoints[2].Position * N[2] + ControlPoints[3].Position * N[3];

			for (int i = 1; i < ControlPoints.Count - 2; ++i)
			{
				const double dt = 1.0 / 100.0;
				for (double t = 0.0; t < 1.0; t += dt)
				{
					N = Algorithms.DeBoor.Evaluate3Regular(t);

					var p = ControlPoints[i - 1].Position * N[0] + ControlPoints[i].Position * N[1] + ControlPoints[i + 1].Position * N[2] + ControlPoints[i + 2].Position * N[3];

					renderer.DrawSegment(lastPoint, p, ColorCode);

					lastPoint = p;
				}
			}
		}
	}
}
