﻿using SaQuCAD.Shapes.Properties;
using SaQuCAD.Algorithms;
using System.Collections.Generic;
using System.Linq;
using SaQuCAD.Maths;
using System;

namespace SaQuCAD.Shapes.Curves
{
	public class InterpolatingBSpline : ControlpointsBase, IEditable
	{

		#region Public Properties

		public const double MinParametrizationInterval = 0.001;

		#endregion

		#region Constructors

		public InterpolatingBSpline(Point[] points, string name = "New Interpolating B-Spline") : base(points)
		{
			Name = name;
		}

		#endregion

		#region Public Methods

		public override void Render(Renderer renderer)
		{
			if (!Visible)
				return;

			if (ControlPoints.Count < 2)
				return;

			foreach (var bezierPolygon in BezierPolygons)
				renderer.DrawBezier(bezierPolygon, ColorCode);

		}

		#endregion

		#region Protected Methods

		protected override void OnChanged()
		{
			base.OnChanged();
			Recalculate();
		}

		#endregion

		#region Private Helpers

		private void Recalculate()
		{
			BezierPolygons.Clear();

			var N = ControlPoints.Count;

			if (N < 3)
				return;

			N--;

			Vector3[] P = ControlPoints.Select(p => p.Position).ToArray();
			double[] d = new double[N];

			for (int i = 0; i < N; ++i)
				d[i] = Math.Max((P[i + 1] - P[i]).Length, MinParametrizationInterval);

			double[] alpha = new double[N];
			double[] beta = new double[N];
			Vector3[] R = new Vector3[N];

			for (int i = 1; i < N; ++i)
			{
				alpha[i] = d[i - 1] / (d[i - 1] + d[i]);
				beta[i] = d[i] / (d[i - 1] + d[i]);
				R[i] = 3.0 * (((P[i + 1] - P[i]) / d[i]) - (P[i] - P[i - 1]) / d[i - 1]) / (d[i - 1] + d[i]);
			}

			double[] diag = new double[N];
			for (int i = 0; i < N; ++i)
				diag[i] = 2.0;

			var x = LinearEquations.SolveTridiagonal(alpha, diag, beta, R);

            for (int i = 0; i < N; ++i)
			{
                double dd = d[i];

                var A = P[i];
                var C = x[i];
                var D = (x[i + 1] - x[i]) / (3.0 * dd);
                var B = (P[i + 1] - P[i] - x[i] * dd * dd - D * dd * dd * dd) / dd;

                var ur = A + B * dd + C * dd * dd + D * dd * dd * dd;

                B *= dd;
                C *= (dd * dd);
                D *= (dd * dd * dd);

                var X = Matrices.CanonicalToBernstein * new Vector4(A.x, B.x, C.x, D.x);
                var Y = Matrices.CanonicalToBernstein * new Vector4(A.y, B.y, C.y, D.y);
                var Z = Matrices.CanonicalToBernstein * new Vector4(A.z, B.z, C.z, D.z);

                BezierPolygons.Add(new Vector3[] { new Vector3(X.x, Y.x, Z.x), new Vector3(X.y, Y.y, Z.y), new Vector3(X.z, Y.z, Z.z), new Vector3(X.w, Y.w, Z.w) });
            }
		}

		#endregion

		#region Private Fields

		private List<Vector3[]> BezierPolygons { get; set; } = new List<Vector3[]>();

		#endregion

	}
}
