﻿using System;

namespace SaQuCAD.Shapes
{
	public interface RaycastView: IDisposable
	{
		Tuple<Vector4, Vector3, double> Cast(double ndcX, double ndcY);
	}

	public abstract class ImplicitMesh : SceneObject
	{
		public abstract RaycastView GetRaycastView(Matrix4x4 projection, Matrix4x4 view);

		private double lightness = 1.0;

		public double Lightness
		{
			get { return lightness; }
			set { lightness = value; OnChanged(); }
		}

	}
}
