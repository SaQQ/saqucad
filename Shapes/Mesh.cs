﻿using SaQuCAD.Maths;
using System;
using System.Collections.Generic;

namespace SaQuCAD.Shapes
{
    public abstract class Mesh : SceneObject
	{

        #region Public Properties
        
        public List<Vector3> Vertices { get; private set; } = new List<Vector3>();

        public List<Tuple<int, int>> Indices { get; private set; } = new List<Tuple<int, int>>();

        #endregion

        #region Public Methods

        public override void Render(Renderer renderer)
        {
            if (!Visible)
                return;

            foreach (var ind in Indices)
                renderer.DrawSegment(Vertices[ind.Item1], Vertices[ind.Item2], ColorCode);
        }

        #endregion

	}
}
