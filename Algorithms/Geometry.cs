﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaQuCAD.Maths;

namespace SaQuCAD.Algorithms
{
    public static class Geometry
    {

        #region Public Methods

        public static double SideOfPoint(Vector2 start, Vector2 end, Vector2 point)
        {
            return start.x * end.y + end.x * point.y + point.x * start.y - point.x * end.y - start.x * point.y - end.x * start.y;
        }

        public static bool Collinear(Vector2 p1, Vector2 p2, Vector2 p3)
        {
            return Math.Abs(SideOfPoint(p1, p2, p3)) < Constants.Eps;
        }

        public static bool PointOnSegment(Vector2 start, Vector2 end, Vector2 point)
        {
            return Math.Min(start.x, end.x) - Constants.Eps <= point.x 
                && point.x <= Math.Max(start.x, end.x) + Constants.Eps
                && Math.Min(start.y, end.y) - Constants.Eps <= point.y
                && point.y <= Math.Max(start.y, end.y) + Constants.Eps
                && Collinear(start, end, point); 
        }

        public static bool DoSegmentsIntersect(Vector2 s1, Vector2 e1, Vector2 s2, Vector2 e2)
        {
            if (PointOnSegment(s1, e1, s2) || PointOnSegment(s1, e1, e2) || PointOnSegment(s2, e2, s1) || PointOnSegment(s2, e2, e1))
                return true;

            if (SideOfPoint(s1, e1, s2) * SideOfPoint(s1, e1, e2) >= 0)
                return false;

            if (SideOfPoint(s2, e2, s1) * SideOfPoint(s2, e2, e1) >= 0)
                return false;

            return true;
        }

        public static bool PointInPolygon(Vector2[] polygon, Vector2 point)
        {
            int N = polygon.Length;

            int intersections = 0;

            var endPoint = new Vector2(polygon.Max(p => p.x) + 1.0, point.y);

            Vector2 tmp = new Vector2();

            for(int i = 0; i < N; ++i)
            {
                var pp = polygon[i > 0 ? i - 1 : N - 1];
                var p = polygon[i];
                var n = polygon[(i + 1) % N];
                var nn = polygon[(i + 2) % N];

				if ((p - n).LengthSquared < Constants.Eps * Constants.Eps)
					continue;

                if (PointOnSegment(p, n, point))
                    return true;

                var pOnSegment = PointOnSegment(point, endPoint, p);
                var nOnSegment = PointOnSegment(point, endPoint, n);

                if (pOnSegment || nOnSegment)
                {
                    if(pOnSegment && nOnSegment)
                    {
                        if (SideOfPoint(point, endPoint, pp) * SideOfPoint(point, endPoint, nn) >= 0 && !Collinear(point, endPoint, pp))
                            continue;

                        ++intersections;
                    }
                    else if (PointOnSegment(point, endPoint, pp) || PointOnSegment(point, endPoint, nn))
                    {
                        continue;
                    }
                    else
                    {
                        if(nOnSegment)
                        {
                            tmp = p;
                        }
                        else
                        {
                            if (SideOfPoint(point, endPoint, tmp) * SideOfPoint(point, endPoint, n) >= 0 && !Collinear(point, endPoint, tmp))
                                continue;

                            ++intersections;
                        }
                    }
                }
                else
                {
                    if (SideOfPoint(point, endPoint, p) * SideOfPoint(point, endPoint, n) >= 0)
                        continue;

                    if (SideOfPoint(p, n, point) * SideOfPoint(p, n, endPoint) >= 0)
                        continue;

                    ++intersections;
                }
            }

            return intersections % 2 == 1;
        }

        #endregion

    }
}
