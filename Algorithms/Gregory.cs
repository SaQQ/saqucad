﻿using System;
using System.Collections.Generic;
using SaQuCAD.Shapes;

namespace SaQuCAD.Algorithms
{
    public static class Gregory
	{

		#region Public Types

		public interface ISideDescriptor
		{
			Point[,] Points { get; }
		}

		#endregion

        [Flags]
		private enum SideParams
        {
            U = 0x01,
            V = 0x02,
            Zero = 0x04,
            One = 0x08,
            Regular = 0x10,
            Inverted = 0x20
        }

        private static SideParams GetSideDescriptor(int fromCornerU, int fromCornerV, int toCornerU, int toCornerV)
        {
            SideParams result = 0;

            if (fromCornerU == toCornerU)
            {
                result |= SideParams.U;

                if (fromCornerU == 0)
                    result |= SideParams.Zero;
                else if (fromCornerU == 1)
                    result |= SideParams.One;
                else throw new ArgumentException("Invalid axis level");

                if (fromCornerV == 0 && toCornerV == 1)
                    result |= SideParams.Regular;
                else if (fromCornerV == 1 && toCornerV == 0)
                    result |= SideParams.Inverted;
                else throw new ArgumentException("Invalid axis orientation");
            }
            else if (fromCornerV == toCornerV)
            {
                result |= SideParams.V;

                if (fromCornerV == 0)
                    result |= SideParams.Zero;
                else if (fromCornerV == 1)
                    result |= SideParams.One;
                else throw new ArgumentException("Invalid axis level");

                if (fromCornerU == 0 && toCornerU == 1)
                    result |= SideParams.Regular;
                else if (fromCornerU == 1 && toCornerU == 0)
                    result |= SideParams.Inverted;
                else throw new ArgumentException("Invalid axis orientation");
            }
            else throw new ArgumentException("Invalid side axis");

            return result;
        }

		private class SideDescriptor : ISideDescriptor
		{
			public SideDescriptor(BezierPatch patch, int fromCornerU, int fromCornerV, int toCornerU, int toCornerV)
			{
				var points = patch.Points;

                var desc = GetSideDescriptor(fromCornerU, fromCornerV, toCornerU, toCornerV);

                if((desc & SideParams.V) > 0)
                    CopyRegularPoints(points, desc);
                else
                    CopyTransposedPoints(points, desc);
			}

            private void CopyRegularPoints(Point[,] points, SideParams desc)
            {
                for (int u = 0; u < 4; ++u)
                    for (int v = 0; v < 4; ++v)
                        Points[(desc & SideParams.Regular) > 0 ? u : 3 - u, (desc & SideParams.Zero) > 0 ? v : 3 - v] = points[u, v];
            }

            private void CopyTransposedPoints(Point[,] points, SideParams desc)
            {
                for (int u = 0; u < 4; ++u)
                    for (int v = 0; v < 4; ++v)
                        Points[(desc & SideParams.Regular) > 0 ? v : 3 - v, (desc & SideParams.Zero) > 0 ? u : 3 - u] = points[u, v];
            }

            public Point[,] Points { get; private set; } = new Point[4, 4];
		}

		private class BezierPatch
		{
			public class Link
			{
                public enum LinkCategory
                {
                    Internal,
                    External
                }

				public Link(Corner from, Corner to, LinkCategory category)
				{
					From = from;
					To = to;
                    Category = category;
				}
				public Corner From { get; private set; }
				public Corner To { get; private set; }
                public LinkCategory Category { get; private set; }

				public bool Visited { get; set; } = false;
				public bool InRecursionStack { get; set; } = false;

				public Link Parent { get; set; } = null;

				public override string ToString()
				{
					return $"({From.UIndex}, {From.VIndex}) - ({To.UIndex}, {To.VIndex})";
                }
			}

			public class Corner
			{
				public Corner(BezierPatch patch, int uc, int vc, Shapes.Point point)
				{
					Patch = patch;
					Point = point;
					UIndex = uc;
					VIndex = vc;
				}

				public BezierPatch Patch { get; private set; }
				public Point Point { get; private set; }

				public int UIndex { get; private set; }
				public int VIndex { get; private set; }

				public List<Link> ExternalLinks { get; private set; } = new List<Link>();
				public List<Link> InternalLinks { get; private set; } = new List<Link>();

				public override string ToString()
				{
					return string.Format("({0}, {1})", UIndex, VIndex);
				}
			}

			public BezierPatch(Shapes.Surfaces.BezierC0 surface, int uPatch, int vPatch)
			{
				Surface = surface;

				for (int u = 0; u < 4; ++u)
					for (int v = 0; v < 4; ++v)
						Points[u, v] = surface.ControlPoints[3 * uPatch + u, 3 * vPatch + v];

				for (int uc = 0; uc < 2; ++uc)
					for (int vc = 0; vc < 2; ++vc)
						Corners[uc, vc] = new Corner(this, uc, vc, Points[uc * 3, vc * 3]);

				for (int uc = 0; uc < 2; ++uc)
				{
					for (int vc = 0; vc < 2; ++vc)
					{
						var corner = Corners[uc, vc];
						corner.InternalLinks.Add(new Link(corner, Corners[(uc + 1) % 2, vc], Link.LinkCategory.Internal));
						corner.InternalLinks.Add(new Link(corner, Corners[uc, (vc + 1) % 2], Link.LinkCategory.Internal));
					}
				}
			}

			public Point[,] Points { get; private set; } = new Shapes.Point[4, 4];

			public Corner[,] Corners { get; private set; } = new Corner[2, 2];

			public Shapes.Surfaces.BezierC0 Surface { get; private set; }

			public bool ContainsPoint(Point point, out int u, out int v)
			{
				for (u = 0; u < 4; ++u)
					for (v = 0; v < 4; ++v)
						if (Points[u, v] == point)
							return true;

				u = v = -1;
				return false;
			}

			public bool ContainsCornerPoint(Point point, out int uc, out int vc)
			{
				for (uc = 0; uc < 2; ++uc)
					for (vc = 0; vc < 2; ++vc)
						if (Corners[uc, vc].Point == point)
							return true;

				uc = vc = -1;
				return false;
			}

		}

		#region Public Methods

		public static List<ISideDescriptor> FindHole(IEnumerable<Shapes.Surfaces.BezierC0> surfaces)
		{
			var patches = ConstructPatchesGraph(surfaces);

			var link = FindLoop(patches);
            if (link == null)
                return null;
            else return ConstructResult(link);
		}

		#endregion

		#region Private Helpers

        private static List<ISideDescriptor> ConstructResult(BezierPatch.Link link)
        {
            List<ISideDescriptor> descriptors = new List<ISideDescriptor>();

            var start = link;
			do
			{
				if (link.Category == BezierPatch.Link.LinkCategory.Internal)
					descriptors.Add(new SideDescriptor(link.From.Patch, link.To.UIndex, link.To.VIndex, link.From.UIndex, link.From.VIndex));
				link = link.Parent;
			} while (link != start);

            return descriptors;
        }

		private static List<BezierPatch> ConstructPatchesGraph(IEnumerable<Shapes.Surfaces.BezierC0> surfaces)
		{
			List<BezierPatch> patches = new List<BezierPatch>();

			foreach (var surface in surfaces)
			{
				int U = surface.ControlPoints.GetLength(0);
				int V = surface.ControlPoints.GetLength(1);

				for (int u = 0; u + 3 < U; u += 3)
					for (int v = 0; v + 3 < V; v += 3)
						patches.Add(new BezierPatch(surface, u / 3, v / 3));
			}

			ConnectPatches(patches);

			RemoveGluedLinks(patches);

			return patches;
		}

		private static BezierPatch.Link FindLoop(List<BezierPatch> patches)
		{
			foreach (var patch in patches)
			{
				for (int uc = 0; uc < 2; ++uc)
				{
					for (int vc = 0; vc < 2; ++vc)
					{
						var corner = patch.Corners[uc, vc];
						foreach (var link in corner.InternalLinks)
						{
							var found = FindLoopFromLink(link, false);
							if (found != null)
								return found;
						}
					}
				}
			}

			return null;
		}

		private static int LoopLength(BezierPatch.Link start)
		{
			int length = 1;

			var link = start.Parent;

			while (link != start)
			{
				++length;
				link = link.Parent;
			}

			return length;
		}

		private static BezierPatch.Link FindLoopFromLink(BezierPatch.Link link, bool nextInternal)
		{
			if (!link.Visited)
			{
				link.Visited = true;
				link.InRecursionStack = true;

				if (nextInternal)
				{
					foreach (var internalLink in link.To.InternalLinks)
					{
						if (!internalLink.Visited)
						{
							internalLink.Parent = link;
							var found = FindLoopFromLink(internalLink, false);
							if (found != null)
								return found;
							internalLink.Parent = null;
						}
						else if (internalLink.InRecursionStack)
						{
							var savedParent = internalLink.Parent;
							internalLink.Parent = link;
							if (LoopLength(link) > 4)
								return internalLink;
							internalLink.Parent = savedParent;
						}
					}
				}
				else
				{
					foreach (var externalLink in link.To.ExternalLinks)
					{
						if (!externalLink.Visited)
						{
							externalLink.Parent = link;
							var found = FindLoopFromLink(externalLink, true);
							if (found != null)
								return found;
							externalLink.Parent = null;
						}
						else if (externalLink.InRecursionStack)
						{
							var savedParent = externalLink.Parent;
							externalLink.Parent = link;
							if (LoopLength(link) > 4)
								return externalLink;
							externalLink.Parent = savedParent;
						}
					}
				}
			}

			link.InRecursionStack = false;

			return null;
		}

		private static void ConnectPatches(List<BezierPatch> patches)
		{
			for (int p1i = 0; p1i < patches.Count; ++p1i)
				for (int p2i = p1i + 1; p2i < patches.Count; ++p2i)
					ConnectPatches(patches[p1i], patches[p2i]);
		}

		private static void ConnectPatches(BezierPatch patch1, BezierPatch patch2)
		{
			for (int uc1 = 0; uc1 < 2; ++uc1)
			{
				for (int vc1 = 0; vc1 < 2; ++vc1)
				{
					var corner1 = patch1.Corners[uc1, vc1];

					int uc2, vc2;
					if (patch2.ContainsCornerPoint(corner1.Point, out uc2, out vc2))
					{
						var corner2 = patch2.Corners[uc2, vc2];
						corner1.ExternalLinks.Add(new BezierPatch.Link(corner1, corner2, BezierPatch.Link.LinkCategory.External));
						corner2.ExternalLinks.Add(new BezierPatch.Link(corner2, corner1, BezierPatch.Link.LinkCategory.External));
					}
				}
			}
		}

		private static List<BezierPatch.Link> FindGluedLinks(BezierPatch.Link link)
		{
			List<BezierPatch.Link> gluedLinks = new List<BezierPatch.Link>();
			foreach(var externalLink in link.To.ExternalLinks)
			{
				foreach(var internalLink in externalLink.To.InternalLinks)
				{
					if (internalLink.To.ExternalLinks.Find(e => e.To == link.From) != null)
						gluedLinks.Add(internalLink);
				}
			}
			return gluedLinks;
		}

		private static int RemoveGluedLinks(List<BezierPatch> patches) 
		{
			int linksRemoved = 0;

			foreach (var patch in patches)
			{
				for (int uc = 0; uc < 2; ++uc)
				{
					for (int vc = 0; vc < 2; ++vc)
					{
						var corner = patch.Corners[uc, vc];

						corner.InternalLinks.RemoveAll(link =>
						{
							var gluedLinks = FindGluedLinks(link);
							if (gluedLinks.Count > 0)
							{
								++linksRemoved;
								foreach (var gluedLink in gluedLinks)
									gluedLink.From.InternalLinks.Remove(gluedLink);
								return true;
							}
							return false;
						});
						
					}
				}
			}

			return linksRemoved;
		}

		#endregion

	}
}
