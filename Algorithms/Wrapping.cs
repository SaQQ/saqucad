﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Algorithms
{
	public static class Wrapping
	{

		#region Public Types

		[Flags]
		public enum WrapType
		{
			None = 0,
			OutOfBounds = 1,
			Min = 2,
			Max = 4,
			Inverted = 8
		}

		[Flags]
		public enum Boundary
		{
			UMin = 1,
			UMax = 2,
			VMin = 4,
			VMax = 8
		}

		public struct WrapDesc
		{

			#region Public Properties

			public WrapType UWrap { get; private set; }

			public WrapType VWrap { get; private set; }

			public bool IsOutOfBounds => Is(UWrap, WrapType.OutOfBounds) || Is(VWrap, WrapType.OutOfBounds);

			public bool IsWrap => !IsOutOfBounds && (Is(UWrap, WrapType.Min) || Is(UWrap, WrapType.Max) || Is(VWrap, WrapType.Min) || Is(VWrap, WrapType.Max));

			#endregion

			#region Constructors

			public WrapDesc(WrapType uWrap, WrapType vWrap)
			{
				UWrap = uWrap;
				VWrap = vWrap;
			}

			#endregion

			#region Public Methods

			public Vector2 ForceWrap(IUVParametrized surf, Vector2 uv)
			{
				if (Is(UWrap, WrapType.Min) || Is(UWrap, WrapType.Max))
				{
					if (Is(UWrap, WrapType.Inverted))
						uv.y = surf.VMax - (uv.y - surf.VMin);
					uv.x = surf.UMax - uv.x;
				}
				if (Is(VWrap, WrapType.Min) || Is(VWrap, WrapType.Max))
				{
					if (Is(VWrap, WrapType.Inverted))
						uv.x = surf.UMax - (uv.x - surf.UMin);
					uv.y = surf.VMax - uv.y;
				}
				return uv;
			}

			#endregion

		}

		public delegate WrapDesc WrapFuncDelegate(ref Vector2 uv);

		private delegate WrapType ParamWrapFuncDelegate(ref Vector2 uv);

		#endregion

		#region Public Methods

		public static bool Is(WrapType type, WrapType expected)
		{
			return (type & expected) == expected;
		}

        public static Boundary OppositeBoundary(Boundary boundary)
        {
            switch(boundary)
            {
                case Boundary.UMax: return Boundary.UMin;
                case Boundary.UMin: return Boundary.UMax;
                case Boundary.VMin: return Boundary.VMax;
                case Boundary.VMax: return Boundary.VMin;
                default: throw new ArgumentException("Boundary composition does not have opposite boundary", nameof(boundary));
            }
        }

		

		public static WrapFuncDelegate CraftWrapFunc(IUVParametrized surf)
		{
			var UWidth = surf.UMax - surf.UMin;
			var VWidth = surf.VMax - surf.VMin;

			ParamWrapFuncDelegate wrapU = (ref Vector2 uv) =>
			{
				if (uv.x < surf.UMin)
				{
					if ((surf.UProperties & ParamProps.Cyclic) == ParamProps.Cyclic)
					{
						bool inv = false;
						while (uv.x < surf.UMin)
						{
							uv.x += UWidth;
							if ((surf.UProperties & ParamProps.Inverted) == ParamProps.Inverted)
							{
								uv.y = surf.VMax - (uv.y - surf.VMin);
								inv = !inv;
							}
						}
						return WrapType.Min | (inv ? WrapType.Inverted : 0);
					}
					else return WrapType.OutOfBounds;
				}
				else if (uv.x > surf.UMax)
				{
					if ((surf.UProperties & ParamProps.Cyclic) == ParamProps.Cyclic)
					{
						bool inv = false;
						while (uv.x > surf.UMax)
						{
							uv.x -= UWidth;
							if ((surf.UProperties & ParamProps.Inverted) == ParamProps.Inverted)
							{
								uv.y = surf.VMax - (uv.y - surf.VMin);
								inv = !inv;
							}
						}
						return WrapType.Max | (inv ? WrapType.Inverted : 0);
					}
					else return WrapType.OutOfBounds;
				}
				else return WrapType.None;
			};

			ParamWrapFuncDelegate wrapV = (ref Vector2 uv) =>
			{
				if (uv.y < surf.VMin)
				{
					if ((surf.VProperties & ParamProps.Cyclic) == ParamProps.Cyclic)
					{
						bool inv = false;
						while (uv.y < surf.VMin)
						{
							uv.y += VWidth;
							if ((surf.VProperties & ParamProps.Inverted) == ParamProps.Inverted)
							{
								uv.x = surf.UMax - (uv.x - surf.UMin);
								inv = !inv;
							}
						}
						return WrapType.Min | (inv ? WrapType.Inverted : 0);
					}
					else return WrapType.OutOfBounds;
				}
				else if (uv.y > surf.VMax)
				{
					if ((surf.VProperties & ParamProps.Cyclic) == ParamProps.Cyclic)
					{
						bool inv = false;
						while (uv.y > surf.VMax)
						{
							uv.y -= VWidth;
							if ((surf.VProperties & ParamProps.Inverted) == ParamProps.Inverted)
							{
								uv.x = surf.UMax - (uv.x - surf.UMin);
								inv = !inv;
							}
						}
						return WrapType.Max | (inv ? WrapType.Inverted : 0);
					}
					else return WrapType.OutOfBounds;
				}
				else return WrapType.None;
			};

			return (ref Vector2 uv) =>
			{
				var uRes = wrapU(ref uv);
				var vRes = wrapV(ref uv);

				return new WrapDesc(uRes, vRes);
			};
		}

		public static Boundary BoundsViolation(IUVParametrized surface, Vector2 uv)
		{
			return
				(uv.x < surface.UMin ? Boundary.UMin : 0)
				| (uv.x > surface.UMax ? Boundary.UMax : 0)
				| (uv.y < surface.VMin ? Boundary.VMin : 0)
				| (uv.y > surface.VMax ? Boundary.VMax : 0);
		}

		public static bool InBounds(IUVParametrized surface, Vector2 uv)
		{
			return BoundsViolation(surface, uv) == 0;
		}

		public static Vector2 BoundaryIntersection(IUVParametrized surf, Vector2 from, Vector2 to, out Boundary resultBound)
		{
			resultBound = 0;

			for(int i = 0; i < 4; ++i)
			{
				var violation = BoundsViolation(surf, to);

				if (violation == 0)
					break;

				if ((violation & Boundary.UMin) == Boundary.UMin)
				{
					var t = (surf.UMin - from.x) / (to.x - from.x);
					to = from + t * (to - from);
					resultBound = Boundary.UMin;
				}
				if ((violation & Boundary.UMax) == Boundary.UMax)
				{
					var t = (surf.UMax - from.x) / (to.x - from.x);
					to = from + t * (to - from);
					resultBound = Boundary.UMax;
				}
				if ((violation & Boundary.VMin) == Boundary.VMin)
				{
					var t = (surf.VMin - from.y) / (to.y - from.y);
					to = from + t * (to - from);
					resultBound = Boundary.VMin;
				}
				if ((violation & Boundary.VMax) == Boundary.VMax)
				{
					var t = (surf.VMax - from.y) / (to.y - from.y);
					to = from + t * (to - from);
					resultBound = Boundary.VMax;
				}

			}

			return to;
		}

		#endregion

	}
}
