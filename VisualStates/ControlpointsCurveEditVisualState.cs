﻿using SaQuCAD.Shapes;
using SaQuCAD.Shapes.Curves;
using System.Linq;
using System.Windows.Input;

namespace SaQuCAD.VisualStates
{

	public partial class ControlpointsCurveEditVisualState : VisualState
	{

		#region Public Properties

		public ControlpointsBase Curve { get; private set; }

		public override string Description { get { return "Editing Controlpoints Curve"; } }

		#endregion

		#region Constructors

		public ControlpointsCurveEditVisualState(Scene scene, Shapes.Curves.ControlpointsBase curve) : base(scene)
		{
			Curve = curve;
		}

		#endregion

		#region Public Methods

		public override void OnEnter()
		{
			Curve.Removed += CurveRemoved;
		}

		public override void OnExit()
		{
			Curve.Removed -= CurveRemoved;
		}

		public override void MouseDown(IVector2 pos)
		{
			didDrag = false;

			dragger = TryClickScenePoint(pos);

			if (dragger == null)
				dragger = TryClickSpecialPoints(pos);

			if (dragger != null)
				Scene.D3Cursor.Position = dragger.CurrentPosition;
		}

		public override void MouseMove(IVector2 pos)
		{
			if (dragger != null)
			{
				var inter = cursorPlaneIntersection(pos);

				if (inter == null)
					return;

				didDrag = true;

				dragger.Drag(inter.Value);

				Scene.D3Cursor.Position = inter.Value;
			}
		}

		public override void MouseUp(IVector2 pos)
		{
			if (dragger == null)
			{
				var inter = cursorPlaneIntersection(pos);

				if (inter != null)
				{
					var newPoint = new Point(inter.Value);
					Scene.AddObject(newPoint);
					Curve.ControlPoints.Add(newPoint);
					Scene.D3Cursor.Position = inter.Value;
				}
			}
			else if (!didDrag && dragger is ScenePointDragger)
			{
				TogglePointInOutCurve((dragger as ScenePointDragger).Point);
			}

			dragger = null;
		}

		public override void KeyUp(Key key)
		{
			if (key == Key.Escape)
				OnExitRequest();
		}

		#endregion

		#region Protected Methods

		protected virtual Dragger TryClickSpecialPoints(IVector2 pos)
		{
			return null;
		}

		#endregion

		#region Private Helpers

		private void CurveRemoved(SceneObject obj)
		{
			OnExitRequest();
		}

		private void TogglePointInOutCurve(Point p)
		{
			if (Curve.ControlPoints.Contains(p))
				Curve.ControlPoints.Remove(p);
			else
				Curve.ControlPoints.Add(p);
		}

		private Dragger TryClickScenePoint(IVector2 pos)
		{
			var scenePoint = TryClickPoints(pos, Scene.SceneObjects.Where(obj => obj is Point).Select(obj => obj as Point));

			if (scenePoint != null)
				return new ScenePointDragger(scenePoint);

			return null;
		}

		#endregion

		#region Private Fields

		private bool didDrag = false;

		private Dragger dragger = null;

		#endregion

	}
}
