﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes;

namespace SaQuCAD.VisualStates
{
	public partial class ControlpointsCurveEditVisualState
	{
		#region Protected Types

		protected abstract class Dragger
		{
			public abstract void Drag(Vector3 to);

			public abstract Vector3 CurrentPosition { get; }
		}

		protected class ScenePointDragger : Dragger
		{
			public Point Point { get; private set; }

			public ScenePointDragger(Point scenePoint)
			{
				Point = scenePoint;
			}

			public override void Drag(Vector3 to)
			{
				Point.Position = to;
			}

			public override Vector3 CurrentPosition { get { return Point.Position; } }
		}

		#endregion
	}
}
