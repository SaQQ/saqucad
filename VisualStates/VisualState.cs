﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes;
using System.Collections.Generic;
using System.Windows.Input;

namespace SaQuCAD.VisualStates
{
	public delegate void ExitRequestEventHandler();

	public abstract class VisualState
	{

		#region Public Properties

		public Scene Scene { get; private set; }

		public abstract string Description { get; }

		public const double PointSelectionTolerance = 10.0;

		public event ExitRequestEventHandler ExitRequest;

		#endregion

		#region Constructors

		public VisualState(Scene scene)
		{
			Scene = scene;
		}

		#endregion

		#region Public Methods

		public virtual void OnEnter() { }

		public virtual void OnExit() { }

		public abstract void MouseDown(IVector2 pos);

		public abstract void MouseUp(IVector2 pos);

		public abstract void MouseMove(IVector2 pos);

		public abstract void KeyUp(Key key);

		public virtual void Render(Renderer renderer)
		{
		}

		#endregion

		#region Protected Methods

		protected virtual void OnExitRequest()
		{
			ExitRequest?.Invoke();
		}

		protected Point TryClickPoints(IVector2 pos, IEnumerable<Point> points)
		{
			foreach (var point in points)
			{
				if (point.Visible && TryClickCoords(pos, point.Position))
					return point;
			}

			return null;
		}

		protected bool TryClickCoords(IVector2 pos, Vector3 coords)
		{
			var proj = Scene.Renderer.PrerenderPoint(coords);

			if (proj.HasValue)
			{
				var dx = proj.Value.x - pos.x;
				var dy = proj.Value.y - pos.y;
				if (dx * dx + dy * dy < PointSelectionTolerance * PointSelectionTolerance)
					return true;
			}

			return false;
		}

		protected Vector3? cursorPlaneIntersection(IVector2 clickPoint)
		{
			var ray = Scene.Renderer.GetViewspaceRay(clickPoint.x, clickPoint.y);

			var viewspaceCursor = Scene.Camera.ViewMatrix * Scene.D3Cursor.Position.AddW();

			Plane plane = new Plane(new Vector3(0.0, 0.0, 1.0), new Vector3(0.0, 0.0, viewspaceCursor.z));

			var inter = plane.RayIntersection(ray);

			if (inter == null)
				return null;
			else return (Scene.Camera.InvertedViewMatrix * inter.Value.AddW()).WithoutW();
		}

		#endregion

	}
}
