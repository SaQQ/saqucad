﻿using SaQuCAD.Shapes;
using SaQuCAD.Shapes.Curves;

namespace SaQuCAD.VisualStates
{
	public sealed class BeizerC0CurveEditVisualState : ControlpointsCurveEditVisualState
	{

		#region Public Properties

		public override string Description { get { return "Editing Piecewise Bezier C0 Curve"; } }

		#endregion

		#region Constructors

		public BeizerC0CurveEditVisualState(Scene scene, BezierC0 bezier) : base(scene, bezier)
		{
		}

		#endregion

	}
}
