﻿using SaQuCAD.Shapes.Curves;

namespace SaQuCAD.VisualStates
{
	public sealed class InterpolatingBSplineCurveEditVisualState : ControlpointsCurveEditVisualState
	{
		#region Public Properties

		public override string Description { get { return "Editing Interpolating BSpline Curve"; } }

		#endregion

		#region Constructors

		public InterpolatingBSplineCurveEditVisualState(Scene scene, InterpolatingBSpline bspline) : base(scene, bspline)
		{
		}

		#endregion
	}
}
