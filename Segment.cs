﻿using SaQuCAD.Maths;

namespace SaQuCAD
{
	public class Segment
	{
		public Vector4 From { get; set; }
		public Vector4 To { get; set; }
	}
}
