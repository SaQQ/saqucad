﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SaQuCAD
{
    public static class KeyState
    {
        private static Dictionary<Key, bool> keyboardStates = new Dictionary<Key, bool>();
        private static Dictionary<MouseButton, bool> mouseStates = new Dictionary<MouseButton, bool>();

        private static Point mousePosition;

        private static UIElement keyboradUiElement;
        private static UIElement mouseUiElement;

        static KeyState()
        {
            foreach (Key k in Enum.GetValues(typeof(Key)))
            {
                if(!keyboardStates.ContainsKey(k))
                    keyboardStates.Add(k, false);
            }

            foreach (MouseButton k in Enum.GetValues(typeof(MouseButton)))
            {
                if (!mouseStates.ContainsKey(k))
                    mouseStates.Add(k, false);
            }
        }

        public static bool GetKeyState(Key key)
        {
            return keyboardStates[key];
        }

        public static Point GetMousePosition()
        {
            return mousePosition;
        }

        public static bool GetMouseState(MouseButton button)
        {
            return mouseStates[button];
        }

        public static void HookEvents(UIElement keyboard, UIElement mouse)
        {
            keyboard.KeyDown += OnKeyDown;
            keyboard.KeyUp += OnKeyUp;
            mouse.MouseDown += OnMouseDown;
            mouse.MouseUp += OnMouseUp;
            mouse.MouseMove += OnMouseMove;

            keyboradUiElement = keyboard;
            mouseUiElement = mouse;
        }

        public static void UnhookEvents(UIElement elem, UIElement mouse)
        {
            elem.KeyDown -= OnKeyDown;
            elem.KeyUp -= OnKeyUp;
            mouse.MouseDown -= OnMouseDown;
            mouse.MouseUp -= OnMouseUp;
            mouse.MouseMove -= OnMouseMove;

            keyboradUiElement = null;
            mouseUiElement = null;
        }

        private static void OnKeyUp(object sender, KeyEventArgs e)
        {
            keyboardStates[e.Key] = false;
        }

        private static void OnKeyDown(object sender, KeyEventArgs e)
        {
            keyboardStates[e.Key] = true;
        }

        private static void OnMouseMove(object sender, MouseEventArgs e)
        {
            mousePosition = e.GetPosition(keyboradUiElement);
        }

        private static void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            mouseStates[e.ChangedButton] = false;
        }

        private static void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            mouseStates[e.ChangedButton] = true;
        }
    }
}
