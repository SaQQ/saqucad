﻿using SaQuCAD.Maths;

namespace SaQuCAD
{
	public abstract class Frustum
	{
		protected Plane[] ClipPlanes { get; set; } = new Plane[0];

		public bool ClipSegment(Segment s)
		{
			var A = s.From.WithoutW();
			var B = s.To.WithoutW();

			foreach (Plane plane in ClipPlanes)
			{
				var distA = A * plane.Normal + plane.Distance;
				var distB = B * plane.Normal + plane.Distance;

				if (distA < 0 && distB < 0)
					return false;
				if (distA >= 0 && distB >= 0)
					continue;

				double t = distA / (distA - distB);

				var intersection = A + t * (B - A);

				if (distA < 0)
					A = intersection;
				else
					B = intersection;
			}

			s.From = A.AddW(s.From.w);
			s.To = B.AddW(s.To.w);

			return true;
		}

		public bool ClipPoint(Vector3 point)
		{
			foreach (Plane plane in ClipPlanes)
			{
				var dist = point * plane.Normal + plane.Distance;

				if (dist < 0.0)
					return false;
			}

			return true;
		}
		
	}
}
