﻿using SaQuCAD.Maths;
using System;

namespace SaQuCAD
{
	public class StereoFrustum : Frustum
	{
		public Matrix4x4 RightProjectionMatrix { get; private set; }
		public Matrix4x4 LeftProjectionMatrix { get; private set; }

		public Matrix4x4 MonoProjectionMatrix { get; private set; }
		public Matrix4x4 InvertedMonoProjectionMatrix { get; private set; }

		private static double clipFov(double iod, double fov, double convergence, double near, double aspect)
		{
			double iod2 = iod / 2.0;
			double horizontalAngle = fov * Math.PI / 360.0;

			var a = Math.Tan(horizontalAngle) * convergence;

			var x = iod2 * convergence / (a + iod2);

			return 2.0 * Math.Atan(a / (convergence - x)) * 180.0 / Math.PI;
		}

		private static double clipNear(double iod, double fov, double convergence, double near, double aspect)
		{
			double iod2 = iod / 2.0;
			double horizontalAngle = fov * System.Math.PI / 360.0;

			var a = System.Math.Tan(horizontalAngle) * convergence;

			var x = iod2 * convergence / (a + iod2);

			return near - x;
		}

		public StereoFrustum(double convergence, double eyeSeparation, double nearDist, double farDist, double aspect, double fov = 45.0)
		{
			double horizontalAngle = fov * System.Math.PI / 360.0;

			var a = System.Math.Tan(horizontalAngle) * convergence;

			var b = a - eyeSeparation / 2.0;
			var c = a + eyeSeparation / 2.0;

			var xmin = -b * nearDist / convergence;
			var xmax = c * nearDist / convergence;

			var ymax = System.Math.Tan(horizontalAngle) * nearDist * aspect;

			LeftProjectionMatrix = Matrix4x4.Frustum(xmin, xmax, -ymax, ymax, nearDist, farDist) * Matrix4x4.Translation(eyeSeparation / 2.0, 0, 0);

			xmin = -c * nearDist / convergence;
			xmax = b * nearDist / convergence;

			RightProjectionMatrix = Matrix4x4.Frustum(xmin, xmax, -ymax, ymax, nearDist, farDist) * Matrix4x4.Translation(-eyeSeparation / 2.0, 0, 0);

			xmin = -a * nearDist / convergence;
			xmax = a * nearDist / convergence;

			MonoProjectionMatrix = Matrix4x4.Frustum(xmin, xmax, -ymax, ymax, nearDist, farDist);
			InvertedMonoProjectionMatrix = MonoProjectionMatrix.Inverse();

			setupClipPlanes(clipNear(eyeSeparation, fov, convergence, nearDist, aspect), farDist, aspect, clipFov(eyeSeparation, fov, convergence, nearDist, aspect));
		}

		private void setupClipPlanes(double nearDist, double farDist, double aspect, double fov)
		{
			double horizontalAngle = fov * System.Math.PI / 360.0;
			var xmax = System.Math.Tan(horizontalAngle) * nearDist;
			var ymax = xmax * aspect;
			double verticalAngle = System.Math.Atan2(ymax, nearDist);

			double sinh = System.Math.Sin(horizontalAngle);
			double sinv = System.Math.Sin(verticalAngle);
			double cosh = System.Math.Cos(horizontalAngle);
			double cosv = System.Math.Cos(verticalAngle);

			Plane left = new Plane(new Vector3(cosh, 0, -sinh), new Vector3(0, 0, 0));
			Plane right = new Plane(new Vector3(-cosh, 0, -sinh), new Vector3(0, 0, 0));
			Plane top = new Plane(new Vector3(0, -cosv, -sinv), new Vector3(0, 0, 0));
			Plane bottom = new Plane(new Vector3(0, cosv, -sinv), new Vector3(0, 0, 0));
			Plane near = new Plane(new Vector3(0, 0, -1), new Vector3(0, 0, -nearDist));
			Plane far = new Plane(new Vector3(0, 0, 1), new Vector3(0, 0, -farDist));

			ClipPlanes = new Plane[] { left, right, top, bottom, near, far };
		}
	}
}
