﻿using SaQuCAD.Algorithms;
using SaQuCAD.Maths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SaQuCAD
{
	public class StereoRenderer : Renderer
	{
		private static int cyanCode = CustomWritableBitmapExtensions.ConvertColor(Colors.Cyan);
		private static int redCode = CustomWritableBitmapExtensions.ConvertColor(Colors.Red);

        private static int multiplyColorCodes(int c1, int c2)
        {
            var a1 = (byte)((c1 >> 24) & 0xFF);
            var r1 = (byte)((c1 >> 16) & 0xFF);
            var g1 = (byte)((c1 >> 8) & 0xFF);
            var b1 = (byte)(c1 & 0xFF);

            var a2 = (byte)((c2 >> 24) & 0xFF);
            var r2 = (byte)((c2 >> 16) & 0xFF);
            var g2 = (byte)((c2 >> 8) & 0xFF);
            var b2 = (byte)(c2 & 0xFF);

            var r3 = (byte)(r1 * r2 / 255);
            var g3 = (byte)(g1 * g2 / 255);
            var b3 = (byte)(b1 * b2 / 255);

            return (a2 << 24) | (r3 << 16) | (g3 << 8) | b3;
        }

		private StereoFrustum frustum;
		private Size size;

		public StereoRenderer(Size viewportSize, double fieldOfView, double eyeSeparation, double convergence)
		{
			size = viewportSize;
			frustum = new StereoFrustum(convergence, eyeSeparation, nearClip, farClip, size.Height / size.Width, fieldOfView);
		}

		private IVector2 toScreenspace(Vector4 pp)
		{
			var xc = (pp.x / 2.0 + 0.5) * size.Width;
			var yc = (pp.y / 2.0 + 0.5) * size.Height;

			return new IVector2((int)xc, (int)(size.Height - yc));
		}

		private IVector2 projectMono(Vector4 viewspacePoint)
		{
			return toScreenspace((frustum.MonoProjectionMatrix * viewspacePoint).WNormalized());
		}

		private IVector2 projectLeft(Vector4 viewspacePoint)
		{
			return toScreenspace((frustum.LeftProjectionMatrix * viewspacePoint).WNormalized());
		}

		private IVector2 projectRight(Vector4 viewspacePoint)
		{
			return toScreenspace((frustum.RightProjectionMatrix * viewspacePoint).WNormalized());
		}

		public override void DrawPoint(Vector3 coords, int colorCode)
		{
			var vp = ViewMatrix * coords.AddW();

			if (!frustum.ClipPoint(vp.WithoutW()))
				return;

			var lsp = projectLeft(vp);
			var rsp = projectRight(vp);

			CustomWritableBitmapExtensions.DrawCircleCustom(context, lsp.x, lsp.y, 3, multiplyColorCodes(redCode, colorCode));
			CustomWritableBitmapExtensions.DrawCircleCustom(context, rsp.x, rsp.y, 3, multiplyColorCodes(cyanCode, colorCode));
		}

		public override void DrawSegment(Vector3 from, Vector3 to, int colorCode)
		{
			Segment s = new Segment { From = ViewMatrix * from.AddW(), To = ViewMatrix * to.AddW() };

			if (!frustum.ClipSegment(s))
				return;

			var slfp = projectLeft(s.From);
			var sltp = projectLeft(s.To);

			CustomWritableBitmapExtensions.DrawLineDDACustom(context, slfp.x, slfp.y, sltp.x, sltp.y, multiplyColorCodes(redCode, colorCode));

			var srfp = projectRight(s.From);
			var srtp = projectRight(s.To);

			CustomWritableBitmapExtensions.DrawLineDDACustom(context, srfp.x, srfp.y, srtp.x, srtp.y, multiplyColorCodes(cyanCode, colorCode));
		}

        public override void DrawBezier(Vector3[] points, int colorCode)
        {
			List<IVector2> clippedFrame = new List<IVector2>();

			if (points.Length < 2)
				return;

			if (points.Length == 2)
			{
				DrawSegment(points[0], points[1], colorCode);
				return;
			}

			for (int i = 0; i < points.Length - 1; ++i)
			{
				Segment s = new Segment { From = ViewMatrix * points[i].AddW(), To = ViewMatrix * points[i + 1].AddW() };
				if (!frustum.ClipSegment(s))
					continue;
				clippedFrame.Add(projectMono(s.From));
				clippedFrame.Add(projectMono(s.To));
			}

			if (clippedFrame.Count == 0)
				return;

			int Mx = clippedFrame.Max(p => p.x);
			int My = clippedFrame.Max(p => p.y);

			int mx = clippedFrame.Min(p => p.x);
			int my = clippedFrame.Min(p => p.y);

//#if DEBUG
//			CustomWritableBitmapExtensions.DrawLineDDACustom(context, mx, my, Mx, my, ColorCodes.RenderRed);
//			CustomWritableBitmapExtensions.DrawLineDDACustom(context, Mx, my, Mx, My, ColorCodes.RenderRed);
//			CustomWritableBitmapExtensions.DrawLineDDACustom(context, Mx, My, mx, My, ColorCodes.RenderRed);
//			CustomWritableBitmapExtensions.DrawLineDDACustom(context, mx, My, mx, my, ColorCodes.RenderRed);
//#endif

			double dt = 1.0 / ((My - my) + (Mx - mx));

			Vector3[] buff = new Vector3[points.Length];

			for (int i = 0; i < buff.Length; ++i)
				buff[i] = points[i];

			DeCasteljau.Evaluate(buff, 0.0);

			Vector3 start = buff[0];

			for (double t = dt; t <= 1.0; t += dt)
			{
				for (int i = 0; i < buff.Length; ++i)
					buff[i] = points[i];

				DeCasteljau.Evaluate(buff, t);

				DrawSegment(start, buff[0], colorCode);

				start = buff[0];
			}

			for (int i = 0; i < buff.Length; ++i)
				buff[i] = points[i];

			DeCasteljau.Evaluate(buff, 1.0);

			DrawSegment(start, buff[0], colorCode);
		}

        public override IVector2? PrerenderPoint(Vector3 point)
		{
			var vp = ViewMatrix * point.AddW();

			if (!frustum.ClipPoint(vp.WithoutW()))
				return null;

			return projectMono(vp);
		}

		public override Tuple<Vector3, Vector3> GetViewspaceRay(double x, double y)
		{
			var nx = ((x / size.Width) - 0.5) * 2.0;
			var ny = (((size.Height - y) / size.Height) - 0.5) * 2.0;

			Vector4 target = (frustum.InvertedMonoProjectionMatrix * new Vector4(nx, ny, 0.0, 1.0)).WNormalized();

			Vector4 origin = new Vector4(0.0, 0.0, 0.0, 1.0);

			return new Tuple<Vector3, Vector3>(origin.WithoutW(), target.WithoutW());
		}

	}
}
