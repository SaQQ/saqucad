﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Linq;
using System.Collections.Generic;
using SaQuCAD.Algorithms;
using SaQuCAD.Maths;

namespace SaQuCAD
{
	public class MonoRenderer : Renderer
	{
		private MonoFrustum frustum;
		private Size size;

		public MonoRenderer(Size viewportSize, double fieldOfView)
		{
			size = viewportSize;
			frustum = new MonoFrustum(nearClip, farClip, size.Height / size.Width, fieldOfView);
		}

		private IVector2 project(Vector4 viewspacePoint)
		{
			var pp = (frustum.ProjectionMatrix * viewspacePoint).WNormalized();

			var xc = (pp.x / 2.0 + 0.5) * size.Width;
			var yc = (pp.y / 2.0 + 0.5) * size.Height;

			return new IVector2((int)xc, (int)(size.Height - yc));
		}

		public override void DrawPoint(Vector3 coords, int colorCode)
		{
			var vp = ViewMatrix * coords.AddW();

			if (!frustum.ClipPoint(vp.WithoutW()))
				return;

			var sp = project(vp);

			CustomWritableBitmapExtensions.DrawCircleCustom(context, sp.x, sp.y, 3, colorCode);
		}

		public override void DrawSegment(Vector3 from, Vector3 to, int colorCode)
		{
			Segment s = new Segment { From = ViewMatrix * from.AddW(), To = ViewMatrix * to.AddW() };

			frustum.ClipSegment(new Segment { From = new Vector4(0, 0, 0.1, 1), To = new Vector4(0, 0, -0.1, 1) });

			if (!frustum.ClipSegment(s))
				return;

			var sfp = project(s.From);
			var stp = project(s.To);

			CustomWritableBitmapExtensions.DrawLineDDACustom(context, sfp.x, sfp.y, stp.x, stp.y, colorCode);
		}

		public override void DrawBezier(Vector3[] points, int colorCode)
		{
			List<IVector2> clippedFrame = new List<IVector2>();

			if (points.Length < 2)
				return;

			if (points.Length == 2)
			{
				DrawSegment(points[0], points[1], colorCode);
				return;
			}

			for (int i = 0; i < points.Length - 1; ++i)
			{
				Segment s = new Segment { From = ViewMatrix * points[i].AddW(), To = ViewMatrix * points[i + 1].AddW() };
				if (!frustum.ClipSegment(s))
					continue;
				clippedFrame.Add(project(s.From));
				clippedFrame.Add(project(s.To));
			}

			if (clippedFrame.Count == 0)
				return;

			int Mx = clippedFrame.Max(p => p.x);
			int My = clippedFrame.Max(p => p.y);

			int mx = clippedFrame.Min(p => p.x);
			int my = clippedFrame.Min(p => p.y);

//#if DEBUG
//			CustomWritableBitmapExtensions.DrawLineDDACustom(context, mx, my, Mx, my, ColorCodes.RenderRed);
//			CustomWritableBitmapExtensions.DrawLineDDACustom(context, Mx, my, Mx, My, ColorCodes.RenderRed);
//			CustomWritableBitmapExtensions.DrawLineDDACustom(context, Mx, My, mx, My, ColorCodes.RenderRed);
//			CustomWritableBitmapExtensions.DrawLineDDACustom(context, mx, My, mx, my, ColorCodes.RenderRed);
//#endif

			double dt = 1.0 / ((My - my) + (Mx - mx));

			Vector3[] buff = new Vector3[points.Length];

			for (int i = 0; i < buff.Length; ++i)
				buff[i] = points[i];

			DeCasteljau.Evaluate(buff, 0.0);

			Vector3 start = buff[0];

			for (double t = dt; t < 1.0; t += dt)
			{
				for (int i = 0; i < buff.Length; ++i)
					buff[i] = points[i];

				DeCasteljau.Evaluate(buff, t);

				DrawSegment(start, buff[0], colorCode);

				start = buff[0];
			}

			for (int i = 0; i < buff.Length; ++i)
				buff[i] = points[i];

			DeCasteljau.Evaluate(buff, 1.0);

			DrawSegment(start, buff[0], colorCode);
		}

		public override IVector2? PrerenderPoint(Vector3 point)
		{
			var vp = ViewMatrix * point.AddW();

			if (!frustum.ClipPoint(vp.WithoutW()))
				return null;

			return project(vp);
		}

		public override Tuple<Vector3, Vector3> GetViewspaceRay(double x, double y)
		{
			var nx = ((x / size.Width) - 0.5) * 2.0;
			var ny = (((size.Height - y) / size.Height) - 0.5) * 2.0;

			Vector4 target = (frustum.InvertedProjectionMatrix * new Vector4(nx, ny, 0.0, 1.0)).WNormalized();

			Vector4 origin = new Vector4(0.0, 0.0, 0.0, 1.0);

			return new Tuple<Vector3, Vector3>(origin.WithoutW(), target.WithoutW());
		}
	}
}
