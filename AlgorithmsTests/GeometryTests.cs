﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaQuCAD.Algorithms;
using SaQuCAD.Maths;

namespace AlgorithmsTests
{

	[TestClass]
	public class GeometryTests
	{

		Vector2[] polygon = new Vector2[]
		{
			new Vector2(0.0, 0.0), new Vector2(1.0, 0.0), new Vector2(1.1, 0.5), new Vector2(1.0, 1.0), new Vector2(0.5, 0.5), new Vector2(0.0, 1.0)
		};

		[TestMethod]
		public void Inside()
		{
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(0.2, 0.2)));
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(0.8, 0.2)));
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(0.5, 0.2)));
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(0.2, 0.8)));
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(0.8, 0.8)));
		}

		[TestMethod]
		public void Outside()
		{
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(-0.2, 0.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(-0.2, 0.8)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(1.2, 0.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(1.2, 0.8)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(-0.2, -0.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(-0.2, 1.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(1.2, -0.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(1.2, 1.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.2, -0.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.5, -0.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.8, -0.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.2, 1.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.5, 1.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.8, 1.2)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.5, 0.6)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.6, 0.7)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.4, 0.7)));
		}

		[TestMethod]
		public void OnEdge()
		{
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(0.2, 0.0)));
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(1.0, 0.2)));
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(0.6, 0.6)));
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(0.4, 0.6)));
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(0.0, 0.2)));
		}

		[TestMethod]
		public void OnVertices()
		{
			foreach(var p in polygon)
				Assert.IsTrue(Geometry.PointInPolygon(polygon, p));
		}

		[TestMethod]
		public void SingularVertex()
		{
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(0.2, 0.5)));
			Assert.IsTrue(Geometry.PointInPolygon(polygon, new Vector2(0.8, 0.5)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(-0.2, 0.5)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(1.2, 0.5)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.5, 1.0)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(-0.2, 1.0)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.2, 1.0)));
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(0.8, 1.0)));
		}

		[TestMethod]
		public void SingularEdge()
		{
			Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(-0.2, 0.0)));
            Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(-10.2, 0.0)));
            Assert.IsFalse(Geometry.PointInPolygon(polygon, new Vector2(1.2, 0.0)));
		}

	}

}
