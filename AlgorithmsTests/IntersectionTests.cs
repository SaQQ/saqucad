﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SaQuCAD.Algorithms;
using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;

namespace AlgorithmsTests
{
    [TestClass]
    public class IntersectionTests
    {

        public const double eps = 0.00000001;

        [TestMethod]
        public void BoundaryIntersection()
        {
            TestIntersection(new SurfaceMock(0.0, 1.0, -2.0, 3.0), new Vector2(0.1, -0.3), 1.0);
        }

        [TestMethod]
        public void BoundaryCloseIntersection()
        {
            TestIntersection(new SurfaceMock(-1.5, 1.0, -1.0, 30.0), new Vector2(0.1, 0.7), eps);
        }

        [TestMethod]
        public void BoundaryCornerIntersection()
        {
            var surf = new SurfaceMock(-1.0, 1.0, -1.0, 1.0);

            Wrapping.Boundary outBound;

            var center = new Vector2(0.0, 0.0);
            var end = new Vector2(-2.0, 1.9);

            var intersection = Wrapping.BoundaryIntersection(surf, center, end, out outBound);

            Assert.IsTrue(OnSegment(center, end, intersection));
            Assert.IsTrue(intersection.x - surf.UMin < eps);
            Assert.AreEqual(Wrapping.Boundary.UMin, outBound);

            end = new Vector2(-1.9, 2.0);

            intersection = Wrapping.BoundaryIntersection(surf, center, end, out outBound);

            Assert.IsTrue(OnSegment(center, end, intersection));
            Assert.IsTrue(intersection.y - surf.VMax < eps);
            Assert.AreEqual(Wrapping.Boundary.VMax, outBound);
        }

        private void TestIntersection(IUVParametrized surf, Vector2 center, double offset = 1.0, int div = 10)
        {
            for (int i = 0; i < div; ++i)
            {
                var pUmin = new Vector2(surf.UMin - offset, surf.VMin + (surf.VMax - surf.VMin) * i / (div - 1));
                var pUmax = new Vector2(surf.UMax + offset, surf.VMin + (surf.VMax - surf.VMin) * i / (div - 1));

                var pVmin = new Vector2(surf.UMin + (surf.UMax - surf.UMin) * i / (div - 1), surf.VMin - offset);
                var pVmax = new Vector2(surf.UMin + (surf.UMax - surf.UMin) * i / (div - 1), surf.VMax + offset);

				Wrapping.Boundary outBound;

                var intersection = Wrapping.BoundaryIntersection(surf, center, pUmin, out outBound);

                Assert.IsTrue(OnSegment(center, pUmin, intersection));
                Assert.IsTrue(intersection.x - surf.UMin < eps);
                Assert.AreEqual(Wrapping.Boundary.UMin, outBound);

                intersection = Wrapping.BoundaryIntersection(surf, center, pUmax, out outBound);

                Assert.IsTrue(OnSegment(center, pUmax, intersection));
                Assert.IsTrue(intersection.x - surf.UMax < eps);
                Assert.AreEqual(Wrapping.Boundary.UMax, outBound);

                intersection = Wrapping.BoundaryIntersection(surf, center, pVmin, out outBound);

                Assert.IsTrue(OnSegment(center, pVmin, intersection));
                Assert.IsTrue(intersection.y - surf.VMin < eps);
                Assert.AreEqual(Wrapping.Boundary.VMin, outBound);

                intersection = Wrapping.BoundaryIntersection(surf, center, pVmax, out outBound);

                Assert.IsTrue(OnSegment(center, pVmax, intersection));
                Assert.IsTrue(intersection.y - surf.VMax < eps);
                Assert.AreEqual(Wrapping.Boundary.VMax, outBound);
            }
        }

        private bool OnLine(Vector2 p0, Vector2 p1, Vector2 point)
        {
            var A = (p1 - p0);
            var B = (point - p0);
            return A.x * B.y - A.y * B.x < eps;
        }

        private bool OnSegment(Vector2 p0, Vector2 p1, Vector2 point)
        {
            return InRange(p0.x, p1.x, point.x) && InRange(p0.y, p1.y, point.y) && OnLine(p0, p1, point);
        }

        private bool InRange(double a, double b, double val)
        {
            return (val >= a && val <= b) || (val >= b && val <= a);
        }

    }
}
