﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;
using System;
using System.Collections.ObjectModel;

namespace AlgorithmsTests
{
	public class SurfaceMock : IUVParametrized
	{

		#region Public Properties

		public int USegments { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }
		public int VSegments { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }

		public double UMin => uMin;

		public double UMax => uMax;

		public double VMin => vMin;

		public double VMax => vMax;

		public ParamProps UProperties => uProperties;

		public ParamProps VProperties => vProperties;

		public ObservableCollection<ITrimmer> Trimmers { get { throw new NotImplementedException(); } }

        public TrimMode TrimMode { get; set; }

        #endregion

        #region Constructors

        public SurfaceMock(double uMin, double uMax, double vMin, double vMax, ParamProps uProperties = 0, ParamProps vProperties = 0)
        {
            this.uMin = uMin;
            this.uMax = uMax;
            this.vMin = vMin;
            this.vMax = vMax;
			this.uProperties = uProperties;
			this.vProperties = vProperties;
        }

        #endregion

        #region Public Methods

        public Vector3 Evaluate(double u, double v)
        {
            throw new NotImplementedException();
        }

        public Vector3 EvaluateUDerivative(double u, double v)
        {
            throw new NotImplementedException();
        }

        public Vector3 EvaluateVDerivative(double u, double v)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Private Data

        private double uMin, uMax, vMin, vMax;
		private ParamProps uProperties, vProperties;

        #endregion

    }
}
