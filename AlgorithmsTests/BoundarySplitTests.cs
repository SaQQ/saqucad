﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SaQuCAD.Algorithms;
using SaQuCAD.Maths;

namespace AlgorithmsTests
{

    [TestClass]
    public class BoundarySplitTests
    {

        [TestMethod]
        public void SplitUMin()
        {
            var surf = new SurfaceMock(-2.0, 3.0, 0.0, 1.0, 0, 0);
            var graph = new Intersections.IntersectionGraph(surf);

            Intersections.IntersectionEdge prevEdge, nextEdge;

            var splitPoint = new Intersections.IntersectionPoint()
            {
                Type = Intersections.PointType.BoundHit,
                Boundary = Wrapping.Boundary.UMin,
                UV = new Vector2(surf.UMin, 0.2)
            };

            graph.SplitBoundaryEdges(splitPoint, out prevEdge, out nextEdge);

            Assert.AreEqual(graph.BoundaryEdges[Wrapping.Boundary.UMin].Count, 2);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.UMin][0], prevEdge);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.UMin][1], nextEdge);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.VMin][0].Next, prevEdge);
            Assert.AreSame(prevEdge.Next, nextEdge);
            Assert.AreSame(nextEdge.Next, graph.BoundaryEdges[Wrapping.Boundary.VMax][0]);

            Assert.AreSame(prevEdge.From, graph.UMinVMin);
            Assert.AreSame(prevEdge.To, splitPoint);
            Assert.AreSame(nextEdge.From, splitPoint);
            Assert.AreSame(nextEdge.To, graph.UMinVMax);
        }

        [TestMethod]
        public void SplitUMax()
        {
            var surf = new SurfaceMock(-2.0, 3.0, 0.0, 1.0, 0, 0);
            var graph = new Intersections.IntersectionGraph(surf);

            Intersections.IntersectionEdge prevEdge, nextEdge;

            var splitPoint = new Intersections.IntersectionPoint()
            {
                Type = Intersections.PointType.BoundHit,
                Boundary = Wrapping.Boundary.UMax,
                UV = new Vector2(surf.UMax, 0.7)
            };

            graph.SplitBoundaryEdges(splitPoint, out prevEdge, out nextEdge);

            Assert.AreEqual(graph.BoundaryEdges[Wrapping.Boundary.UMax].Count, 2);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.UMax][0], prevEdge);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.UMax][1], nextEdge);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.VMax][0].Next, prevEdge);
            Assert.AreSame(prevEdge.Next, nextEdge);
            Assert.AreSame(nextEdge.Next, graph.BoundaryEdges[Wrapping.Boundary.VMin][0]);

            Assert.AreSame(prevEdge.From, graph.UMaxVMax);
            Assert.AreSame(prevEdge.To, splitPoint);
            Assert.AreSame(nextEdge.From, splitPoint);
            Assert.AreSame(nextEdge.To, graph.UMaxVMin);
        }

        [TestMethod]
        public void SplitVMin()
        {
            var surf = new SurfaceMock(-2.0, 3.0, 0.0, 1.0, 0, 0);
            var graph = new Intersections.IntersectionGraph(surf);

            Intersections.IntersectionEdge prevEdge, nextEdge;

            var splitPoint = new Intersections.IntersectionPoint()
            {
                Type = Intersections.PointType.BoundHit,
                Boundary = Wrapping.Boundary.VMin,
                UV = new Vector2(0.0, surf.VMin)
            };

            graph.SplitBoundaryEdges(splitPoint, out prevEdge, out nextEdge);

            Assert.AreEqual(graph.BoundaryEdges[Wrapping.Boundary.VMin].Count, 2);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.VMin][0], prevEdge);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.VMin][1], nextEdge);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.UMax][0].Next, prevEdge);
            Assert.AreSame(prevEdge.Next, nextEdge);
            Assert.AreSame(nextEdge.Next, graph.BoundaryEdges[Wrapping.Boundary.UMin][0]);

            Assert.AreSame(prevEdge.From, graph.UMaxVMin);
            Assert.AreSame(prevEdge.To, splitPoint);
            Assert.AreSame(nextEdge.From, splitPoint);
            Assert.AreSame(nextEdge.To, graph.UMinVMin);
        }

        [TestMethod]
        public void SplitVMax()
        {
            var surf = new SurfaceMock(-2.0, 3.0, 0.0, 1.0, 0, 0);
            var graph = new Intersections.IntersectionGraph(surf);

            Intersections.IntersectionEdge prevEdge, nextEdge;

            var splitPoint = new Intersections.IntersectionPoint()
            {
                Type = Intersections.PointType.BoundHit,
                Boundary = Wrapping.Boundary.VMax,
                UV = new Vector2(0.1, surf.VMax)
            };

            graph.SplitBoundaryEdges(splitPoint, out prevEdge, out nextEdge);

            Assert.AreEqual(graph.BoundaryEdges[Wrapping.Boundary.VMax].Count, 2);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.VMax][0], prevEdge);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.VMax][1], nextEdge);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.UMin][0].Next, prevEdge);
            Assert.AreSame(prevEdge.Next, nextEdge);
            Assert.AreSame(nextEdge.Next, graph.BoundaryEdges[Wrapping.Boundary.UMax][0]);

            Assert.AreSame(prevEdge.From, graph.UMinVMax);
            Assert.AreSame(prevEdge.To, splitPoint);
            Assert.AreSame(nextEdge.From, splitPoint);
            Assert.AreSame(nextEdge.To, graph.UMaxVMax);
        }

        [TestMethod]
        public void DoubleSplitUMin()
        {
            var surf = new SurfaceMock(-2.0, 3.0, 0.0, 1.0, 0, 0);
            var graph = new Intersections.IntersectionGraph(surf);

            Intersections.IntersectionEdge prevEdge, nextEdge;

            var splitPoint0 = new Intersections.IntersectionPoint()
            {
                Type = Intersections.PointType.BoundHit,
                Boundary = Wrapping.Boundary.UMin,
                UV = new Vector2(surf.UMin, 0.2)
            };

            var splitPoint1 = new Intersections.IntersectionPoint()
            {
                Type = Intersections.PointType.BoundHit,
                Boundary = Wrapping.Boundary.UMin,
                UV = new Vector2(surf.UMin, 0.4)
            };

            graph.SplitBoundaryEdges(splitPoint0, out prevEdge, out nextEdge);
            graph.SplitBoundaryEdges(splitPoint1, out prevEdge, out nextEdge);

            var splittedEdge = graph.BoundaryEdges[Wrapping.Boundary.UMin];

            Assert.AreEqual(splittedEdge.Count, 3);
            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.VMin][0].Next, splittedEdge[0]);
            Assert.AreSame(splittedEdge[0].Next, splittedEdge[1]);
            Assert.AreSame(splittedEdge[1].Next, splittedEdge[2]);
            Assert.AreSame(splittedEdge[2].Next, graph.BoundaryEdges[Wrapping.Boundary.VMax][0]);

            Assert.AreSame(splittedEdge[0].From, graph.UMinVMin);
            Assert.AreSame(splittedEdge[0].To, splitPoint0);
            Assert.AreSame(splittedEdge[1].From, splitPoint0);
            Assert.AreSame(splittedEdge[1].To, splitPoint1);
            Assert.AreSame(splittedEdge[2].From, splitPoint1);
            Assert.AreSame(splittedEdge[2].To, graph.UMinVMax);
        }

        [TestMethod]
        public void CornerSplitUMinVMax()
        {
            var surf = new SurfaceMock(-2.0, 3.0, 0.0, 1.0, 0, 0);
            var graph = new Intersections.IntersectionGraph(surf);

            Intersections.IntersectionEdge prevEdge, nextEdge;

            var splitPoint0 = new Intersections.IntersectionPoint()
            {
                Type = Intersections.PointType.BoundHit,
                Boundary = Wrapping.Boundary.UMin,
                UV = new Vector2(surf.UMin, 0.7)
            };

            var splitPoint1 = new Intersections.IntersectionPoint()
            {
                Type = Intersections.PointType.BoundHit,
                Boundary = Wrapping.Boundary.VMax,
                UV = new Vector2(0.1, surf.VMax)
            };

            graph.SplitBoundaryEdges(splitPoint0, out prevEdge, out nextEdge);
            graph.SplitBoundaryEdges(splitPoint1, out prevEdge, out nextEdge);

            var uMin = graph.BoundaryEdges[Wrapping.Boundary.UMin];
            var vMax = graph.BoundaryEdges[Wrapping.Boundary.VMax];

            Assert.AreEqual(uMin.Count, 2);
            Assert.AreEqual(vMax.Count, 2);

            Assert.AreSame(graph.BoundaryEdges[Wrapping.Boundary.VMin][0].Next, uMin[0]);
            Assert.AreSame(uMin[0].Next, uMin[1]);
            Assert.AreSame(uMin[1].Next, vMax[0]);
            Assert.AreSame(vMax[0].Next, vMax[1]);
            Assert.AreSame(vMax[1].Next, graph.BoundaryEdges[Wrapping.Boundary.UMax][0]);

            Assert.AreSame(uMin[0].From, graph.UMinVMin);
            Assert.AreSame(uMin[0].To, splitPoint0);
            Assert.AreSame(uMin[1].From, splitPoint0);
            Assert.AreSame(uMin[1].To, graph.UMinVMax);
            Assert.AreSame(vMax[0].From, graph.UMinVMax);
            Assert.AreSame(vMax[0].To, splitPoint1);
            Assert.AreSame(vMax[1].From, splitPoint1);
            Assert.AreSame(vMax[1].To, graph.UMaxVMax);
        }

    }

}
