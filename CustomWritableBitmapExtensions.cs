﻿using System.Windows.Media.Imaging;

namespace System.Windows.Media.Imaging
{
	public unsafe static class CustomWritableBitmapExtensions
	{
		public static int ConvertColor(Color color)
		{
			var col = 0;

			if (color.A != 0)
			{
				var a = color.A + 1;
				col =
					(color.A << 24)
				  | ((byte)((color.R * a) >> 8) << 16)
				  | ((byte)((color.G * a) >> 8) << 8)
				  | ((byte)((color.B * a) >> 8));
			}

			return col;
		}

        public static Color ConvertColor(int code)
        {
            var a = (code >> 24) & 0xFF;
            var r = (code >> 16) & 0xFF;
            var g = (code >>  8) & 0xFF;
            var b = (code >>  0) & 0xFF;
            return Color.FromArgb((byte)a, (byte)r, (byte)g, (byte)b);
        }

		public static int addColors(int dst, int src)
		{
            byte sa = (byte)((src >> 24) & 0xFF);
            byte sr = (byte)((src >> 16) & 0xFF);
            byte sg = (byte)((src >> 8) & 0xFF);
            byte sb = (byte)(src & 0xFF);

            byte da = (byte)((dst >> 24) & 0xFF);
			byte dr = (byte)((dst >> 16) & 0xFF);
			byte dg = (byte)((dst >> 8) & 0xFF);
			byte db = (byte)(dst & 0xFF);

            float a = sa / 255;

            var ra = (byte)Math.Min(a * sa + (1 - a) * da, 0xFF);
			var rr = (byte)Math.Min(a * sr + (1 - a) * dr, 0xFF);
			var rg = (byte)Math.Min(a * sg + (1 - a) * dg, 0xFF);
			var rb = (byte)Math.Min(a * sb + (1 - a) * db, 0xFF);

			return (ra << 24) | (rr << 16) | (rg << 8) | rb;
		}

		public static void DrawLineDDACustom(BitmapContext context, int x1, int y1, int x2, int y2, int colorCode)
		{
			// Use refs for faster access (really important!) speeds up a lot!
			var w = context.Width;
			var h = context.Height;
			var pixels = context.Pixels;

			// Distance start and end point
			int dx = x2 - x1;
			int dy = y2 - y1;

			// Determine slope (absoulte value)
			int len = dy >= 0 ? dy : -dy;
			int lenx = dx >= 0 ? dx : -dx;
			if (lenx > len)
			{
				len = lenx;
			}

			// Prevent divison by zero
			if (len != 0)
			{
				// Init steps and start
				float incx = dx / (float)len;
				float incy = dy / (float)len;
				float x = x1;
				float y = y1;

				// Walk the line!
				for (int i = 0; i < len; i++)
				{
					if (y < h && y >= 0 && x < w && x >= 0)
					{
						pixels[(int)y * w + (int)x] = addColors(pixels[(int)y * w + (int)x], colorCode);
					}
					x += incx;
					y += incy;
				}
			}
		}

		public static void DrawCircleCustom(BitmapContext context, int xc, int yc, int r, int colorCode)
		{
			var pixels = context.Pixels;
			int w = context.Width;
			int h = context.Height;

			// Avoid endless loop
			if (r < 1)
			{
				return;
			}

			// Init vars
			int uh, lh, uy, ly, lx, rx;
			int x = r;
			int y = 0;
			int xrSqTwo = (r * r) << 1;
			int yrSqTwo = (r * r) << 1;
			int xChg = r * r * (1 - (r << 1));
			int yChg = r * r;
			int err = 0;
			int xStopping = yrSqTwo * r;
			int yStopping = 0;
           

			// Draw first set of points counter clockwise where tangent line slope > -1.
			while (xStopping >= yStopping)
			{
				// Draw 4 quadrant points at once
				// Upper half
				uy = yc + y;
				// Lower half
				ly = yc - y;

				// Clip
				if (uy < 0) uy = 0;
				if (uy >= h) uy = h - 1;
				if (ly < 0) ly = 0;
				if (ly >= h) ly = h - 1;

				// Upper half
				uh = uy * w;
				// Lower half
				lh = ly * w;

				rx = xc + x;
				lx = xc - x;

				// Clip
				if (rx < 0) rx = 0;
				if (rx >= w) rx = w - 1;
				if (lx < 0) lx = 0;
				if (lx >= w) lx = w - 1;

				// Draw line
				for (int i = lx; i <= rx; i++)
				{
					// Quadrant II to I (Actually two octants)
					pixels[i + uh] = addColors(pixels[i + uh], colorCode);

					// Quadrant III to IV
					pixels[i + lh] = addColors(pixels[i + lh], colorCode);
				}

				y++;
				yStopping += xrSqTwo;
				err += yChg;
				yChg += xrSqTwo;
				if ((xChg + (err << 1)) > 0)
				{
					x--;
					xStopping -= yrSqTwo;
					err += xChg;
					xChg += yrSqTwo;
				}
			}

			// ReInit vars
			x = 0;
			y = r;

			// Upper half
			uy = yc + y;
			// Lower half
			ly = yc - y;

			// Clip
			if (uy < 0) uy = 0;
			if (uy >= h) uy = h - 1;
			if (ly < 0) ly = 0;
			if (ly >= h) ly = h - 1;

			// Upper half
			uh = uy * w;
			// Lower half
			lh = ly * w;

			xChg = r * r;
			yChg = r * r * (1 - (r << 1));
			err = 0;
			xStopping = 0;
			yStopping = xrSqTwo * r;

			// Draw second set of points clockwise where tangent line slope < -1.
			while (xStopping <= yStopping)
			{
				// Draw 4 quadrant points at once
				rx = xc + x;
				lx = xc - x;

				// Clip
				if (rx < 0) rx = 0;
				if (rx >= w) rx = w - 1;
				if (lx < 0) lx = 0;
				if (lx >= w) lx = w - 1;

				// Draw line
				for (int i = lx; i <= rx; i++)
				{
					pixels[i + uh] = addColors(pixels[i + uh], colorCode); // Quadrant II to I (Actually two octants)
					pixels[i + lh] = addColors(pixels[i + lh], colorCode); // Quadrant III to IV
				}

				x++;
				xStopping += yrSqTwo;
				err += xChg;
				xChg += yrSqTwo;
				if ((yChg + (err << 1)) > 0)
				{
					y--;
					uy = yc + y; // Upper half
					ly = yc - y; // Lower half
					if (uy < 0) uy = 0; // Clip
					if (uy >= h) uy = h - 1; // ...
					if (ly < 0) ly = 0;
					if (ly >= h) ly = h - 1;
					uh = uy * w; // Upper half
					lh = ly * w; // Lower half
					yStopping -= xrSqTwo;
					err += yChg;
					yChg += xrSqTwo;
				}
			}
		}
	}

}
