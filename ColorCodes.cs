﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SaQuCAD
{
    public static class ColorCodes
    {

        #region Codes

        private static int CornflowerBlue = CustomWritableBitmapExtensions.ConvertColor(Colors.CornflowerBlue);
        private static int Red = CustomWritableBitmapExtensions.ConvertColor(Colors.Red);
        private static int DarkRed = CustomWritableBitmapExtensions.ConvertColor(Colors.DarkRed);
        private static int Green = CustomWritableBitmapExtensions.ConvertColor(Colors.Green);
        private static int Blue = CustomWritableBitmapExtensions.ConvertColor(Colors.Blue);
        private static int White = CustomWritableBitmapExtensions.ConvertColor(Colors.White);
        private static int Gold = CustomWritableBitmapExtensions.ConvertColor(Colors.Gold);
        private static int Goldenrod = CustomWritableBitmapExtensions.ConvertColor(Colors.Goldenrod);
        private static int LightGray = CustomWritableBitmapExtensions.ConvertColor(Colors.LightGray);
        private static int DarkGray = CustomWritableBitmapExtensions.ConvertColor(Colors.DarkGray);
        private static int Yellow = CustomWritableBitmapExtensions.ConvertColor(Colors.Yellow);
        private static int Black = CustomWritableBitmapExtensions.ConvertColor(Colors.Black);

        #endregion

        #region Function Colors

        public static int RenderBackground => renderMode == Mode.Dark ? Black : White;
        public static int RenderPrimary => renderMode == Mode.Dark ? White : Black;
        public static int RenderSelected => renderMode == Mode.Dark ? Gold : Goldenrod;
        public static int RenderFrame => renderMode == Mode.Dark ? CornflowerBlue : CornflowerBlue;
        public static int RenderRed => renderMode == Mode.Dark ? Red : Red;
        public static int RenderGreen => renderMode == Mode.Dark ? Green : Green;
        public static int RenderBlue => renderMode == Mode.Dark ? Blue : Blue;

        #endregion

        #region Modes Machinery

        public delegate void ModeChanged();

        public static event ModeChanged RenderModeChanged;

        public enum Mode
        {
            Light, Dark
        }

        public static Mode RenderMode
        {
            get { return renderMode; }
            set { renderMode = value; RenderModeChanged(); }
        }

        #endregion

        #region Private Fields

        private static Mode renderMode = Mode.Dark;

        #endregion

    }
}
