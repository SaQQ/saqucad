﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Maths
{
	public static class Matrices
	{

		#region Polynomials Bases

		public static Matrix4x4 CanonicalToBernstein = new Matrix4x4()
		{
			v11 = 1.0, v12 = 0.0,       v13 = 0.0,       v14 = 0.0,
            v21 = 1.0, v22 = 1.0 / 3.0, v23 = 0.0,       v24 = 0.0,
            v31 = 1.0, v32 = 2.0 / 3.0, v33 = 1.0 / 3.0, v34 = 0.0,
            v41 = 1.0, v42 = 1.0,       v43 = 1.0,       v44 = 1.0
		};

		public static Matrix4x4 BernsteinToCanonical = new Matrix4x4()
		{
			v11 =  1.0, v12 =  0.0, v13 =  0.0, v14 = 0.0,
            v21 = -3.0, v22 =  3.0, v23 =  0.0, v24 = 0.0,
            v31 =  3.0, v32 = -6.0, v33 =  3.0, v34 = 0.0,
            v41 = -1.0, v42 =  3.0, v43 = -3.0, v44 = 1.0
		};

		#endregion

	}
}
